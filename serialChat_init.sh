#!/bin/bash
# /etc/init.d/serialChat_init
# attention screen doit etre installe sur l'ordinateur
#version 0.1 2014-15-27
#0.1 debut d'une ebauche de script init

### BEGIN INIT INFO
# Provides:             serialChat
# Required-Start:       $local_fs $remote_fs
# Required-Stop:        $local_fs $remote_fs
# Should-Start:         $network
# Should-Stop:          $network
# Default-Start:        5
# default-Stop:         0 1 6 2 3 4 
# Short-Description:    serialChat
# Description:          Starts the SerialChat computer interface
### END INIT INFO


# Settings
SERVICE='serialChat.jar'
OPTIONS='noGui noEcho'
USERNAME='root'
EXECPATH='/home/serialChat'
HISTORY=1024
CPU_COUNT=2
INVOCATION="java -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=$CPU_COUNT -XX:+AggressiveOpts -jar $SERVICE $OPTIOMS"

ME=`whoami`
as_user() {
    if [ $ME == $USERNAME ]; then
        bash -c "$1"
    else
        su - $USERNAME -c "$1"
    fi
}

sc_start() {
    if pgrep -u $USERNAME -f $SERVICE > /dev/null
    then
        echo "$SERVICE est deja en cours d'execution!"
    else
        echo "Lancement de $SERVICE..."
        cd $EXECPATH
        #as_user "cd $EXECPATH && screen -h $HISTORY -dmS serialChat $INVOCATION"
        as_user "cd $EXECPATH && screen -h $HISTORY -dmS serialChat $INVOCATION"
        sleep 7
        if pgrep -u $USERNAME -f $SERVICE > /dev/null
        then
            echo "$SERVICE est maintenant en cours d'execution."
        else
            echo "Erreur! Impossible de lancer $SERVICE!"
        fi
    fi
}

sc_stop() {
echo "fait chier"
}

#Start-Stop here
case "$1" in
    start)
        sc_start
        ;;
    stop)
        echo "salut"
        sc_stop
        ;;
    restart)
        echo "salut2"
        sc_stop
        sc_start
        ;;
    status)
      if pgrep -u $USERNAME -f $SERVICE > /dev/null
      then
        echo "$SERVICE is running."
      else
        echo "$SERVICE is not running."
      fi
        ;;
    *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
        ;;
esac

exit 0