## Outils et logiciels utilisés



### 1. NetBeans
---------------------

NetBeans est un Environnement de Développement Intégré (IDE), qui a été
développé à l'origine par Sun Microsystems[^4] et qui est maintenant
placé en open source. Il s'agit d'un IDE qui permet de développer
notamment en Java[^5], C[^6], C++[^7], PHP[^8] et HTML[^9]. Dans le
cadre de ce stage NetBeans a été utilisé pour développer l'application
principale en Java.

Cet IDE est particulièrement efficace et simple à prendre en main pour
effectuer un développement Java. Il permet notamment la gestion complète
d'un projet en permettant d'effectuer la compilation, d'utiliser le
debuggeur Java et de fabriquer la documentation associée à votre projet
en utilisant JavaDoc[^10].

<img src="images/NetBeans_interface.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*3. illustration: Interface de NetBeans*<br>



### 2. Liaisons séries RS-232 et boucle de courant
--------------------------------------------------------

Cette partie aborde les notions utiles à la compréhension des différents
montages nécessaires à l\'interconnexion des terminaux ainsi que du
convertisseur qui aurait été utilisé pour pouvoir connecter les
terminaux électromécaniques.

#### Précisions sur la liaison série

Le port série est une interface hardware permettant de transmettre des
informations de manière séquentielle (les unes après les autres). Le
mode de transmission sur une ligne série appelée RS-232 est décrit dans
l'illustration 3. Les bits sont codés de la manière suivante :

-   \+ 5V pour un bit à 0
-    - 5V pour un bit à 1
-   Le bit de start est codé par un bit à 0
-   Le bit de stop est codé par un bit à 1

Le protocole RS-232 correspond en fait à deux protocoles des normes
internationales CCITT[^11]. D'une part le protocoles V24 qui définit
l\'échange de données entre un équipement terminal (ETTD[^12] ou
DTE[^13]) et un équipement de communication (ETCD[^14] ou DCE[^15]). La
jonction V24 comprend un certain nombre de plans de câblage utilisés
pour gérer la connexion de deux équipements entre eux. D'autre part le
protocole V28 qui énonce les seuils de tension pour chaque niveau
logique qui sont énoncés plus haut.

<img src="images/chronograme_liason_serie.gif" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*4. Illustration: Chronogramme d\'une communication série*<br>

De plus les différentes façons de connecter les périphériques diffèrent,
dans le cas où l'on connecte un périphérique terminal avec un équipement
de communication le câble sera dit droit alors que si l'on connecte deux
périphériques entre eux le câble sera dit croisé selon le plan de
câblage de la l'[illustration 5](#images8|graphic).

<img src="images/cable_croise.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*5. Illustration: Plan de câblage pour connecter deux périphériques terminaux entre eux*<br>

<img src="images/brochage_RS232.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*6. Illustration: Brochage d\'une liaison série*<br>



#### Précisions sur la boucle de courant

La boucle de courant est une méthode permettant de communiquer entre
deux périphériques en utilisant un courant à la place d'une tension pour
signaler les bits. Cette méthode a pour avantage de ne pas être trop
impactée par les pertes de la ligne. Lors de son invention cette méthode
permettait de connecter plusieurs terminaux grâce à une seule boucle de
courant. Ce mode transmission est toujours utilisé de nos jour dans
l'industrie et dans le standard MIDI[^16].

#### Convertisseur boucle de courant RS-232

Cette « boite rouge » provenait d'un don fait à l'association ACONIT. Il
s'agit d'un convertisseur boucle de courant vers RS-232C. Ce
convertisseur dispose d'une entrée RS-232 et de deux entrées « boucle de
courant ». A l'origine ce boîtier devait servir à connecter de vieux
terminaux qui ne pouvait communiquer que via boucle de courant avec des
terminaux plus récents. Mais suite aux pannes diverses de ces terminaux
électromécaniques, il n'a malheureusement pas été possible de connecter
ces terminaux au système.


<img src="images/red_box0001.JPG" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*7. Illustration: Boîtier de conversion RS-232C boucle de courant
(collection
ACONIT)*<br>


Ce boîtier initialement en panne, a été réparé et a servi à tester les
terminaux dans l'espoir d'en découvrir un en état de fonctionnement.

<img src="images/shema_boucle_de_courant_RS232.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*8. Illustration: Schéma du convertisseur boucle de courant ↔
RS232*<br>


À l'origine le convertisseur devait servir à connecter deux terminaux
électromécaniques : un ASR-33 et un M-43. Le premier est un des premiers
terminaux à utiliser le nouveau code ASCII, et a été fabriqué de 1975 à
1981 à presque un million d'exemplaires. Le second utilise une
imprimante à aiguille.

<img src="images/teletype.jpg" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*9. Illustration: ASR-33 (collection
ACONIT)*<br>


### 3. Minitel 1
----------------------

#### Description

Le Médium Interactif par Numérisation d'Information TÉLéphonique ou plus
simplement Minitel est un type de terminal informatique qui permettait
de se connecter au service français de vidéotex[^17], Télétel. Le
service Télétel a été exploité en France de 1980 à 2012. Lors du
lancement du Minitel il s\'agissait d'une technologie de pointe, et
certaines personnes pensaient que cette technologie allait supplanter
Internet.

<img src="images/minitel.JPG" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*11. Illustration: Minitel 1 Alcatel (collection ACONIT)*<br>

#### Communication entre le Minitel et un Ordinateur

Le Minitel dispose d'un connecteur de type DIN cinq broches qui
permettait à l'origine de connecter une imprimante sur le Minitel pour
pouvoir imprimer le contenu de l\'écran du Minitel sur un support
papier. Cette imprimante communiquait avec le Minitel via une liaison
série, et assez vite ce port d'extension fut détourné de son utilisation
première pour par exemple utiliser un Minitel comme un terminal série.

<img src="images/minitel_adapt.JPG" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*12. Illustration: Boîtier d\'adaptation (collection
ACONIT)*<br>


Mais la liaison série implémentée par le Minitel ne respecte pas la
norme V28 car les signaux sont codés grâce aux niveaux TTL (0V 5V). Ces
niveaux sont normalement compris par les adaptateurs séries modernes,
mais pour réellement respecter la norme, il faut adapter les signaux de
type TTL en signaux respectant la norme V28. Il existe des puces conçues
pour cet usage notamment le très connu MAX-232.

J'ai donc réalisé un petit montage pour pouvoir adapter les signaux. Ce
montage a d\'abord été construit sous forme de prototype puis un produit
finale qui ce présente sous la forme d'une petite boite qui a été
réalisé pour pouvoir être installé dans une exposition où des personnes
sont susceptibles de l\'abîmer. Ce montage est auto alimenté grâce à
l'alimentation délivrée par le Minitel. Ce dernier délivre 9 Volt et 1
Ampère qui sont largement suffisants pour alimenter le circuit
d'adaptation. Cette tension est ramenée à 5 Volts par un régulateur de
tension LM-7805 pour pouvoir alimenter le circuit intégré. Ce montage
permet de connecter un Minitel comme un périphérique communication
classique ce qui permet d'utiliser des câbles qui sont dits droits.

<img src="images/minitel.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*13. Illustration: Circuit d\'adaptation Minitel ↔
Ordinateur*<br>

Les caractéristiques propres à la liaison série du Minitel
sont les suivantes :

-   7 Bits de données
-   1 Bit de parité paire
-   1 Bit de stop
-   1200 Bauds
-   Communication asynchrone

### 4. TeleVideo 920C
----------------------------

#### Description

Les terminaux TeleVideo[^18] TVI-920 sont des terminaux compacts. Ce
sont les premiers terminaux commercialisés par TeleVideo. Il s'agissait
de terminaux compatibles avec la majorité des ordinateurs de l\'époque,
qui étaient assez abordables et qui pouvaient communiquer via boucle de
courant ou via RS-232C. Ces terminaux disposaient d'un grand avantage
par rapport aux terminaux plus anciens qui ne disposaient pas d\'écran,
mais qui avaient à la place une imprimante qui retranscrivait ce qui
était affiché sur l\'écran du TVI-920C sur un support papier. Le
TVI-920C avait un écran ce qui avait pour avantage de permettre
d\'éditer des lignes directement sur l'ordinateur. Ces TVI-920C
disposaient quand même d'une prise permettant de connecter une
imprimante pour pouvoir retranscrire ce qui était affiché sur l\'écran
vers un listing en papier, un support plus pérenne.

<img src="images/tvi920.JPG" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*14. Illustration: TVI-920C (collection
ACONIT)*<br>


#### Communication entre le TVI-920C et un ordinateur

Le TVI-920C dispose sur sa face arrière de deux prises de type Canon 25
de nos jours plus connues sous le nom DB-25 qui permettent de connecter
une imprimante et de connecter le terminal à un ordinateur. Cette
dernière contient le brochage classique pour une communication RS-232 et
en plus sur certaines des broches qui sont inutilisées se trouve la
réception et l\'émission en boucle de courant. Le terminal étant comme
l\'ordinateur un équipement dit terminal il a fallut réaliser un câble
croisé pour pouvoir inter-connecter les deux équipements. Ce câble
permet aussi de re-boucler la broche 20 avec la broche 6, respectivement
« connectez le poste de données, DTR» et la « poste de données prêt,
DSR » du côté de l'ordinateur pour faire croire à celui-ci qu'il y a un
périphérique de communication présent connecté au bout de la ligne. Si
ce câble n'est pas connecté l'ordinateur pourra recevoir des
informations de la part du périphérique mais ne pourra pas émettre à
destination du terminal.

<img src="images/adapt_tvi.JPG" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*15. Illustration: Adaptateur d\'interconnexion TVI-920C ↔
Ordinateur*<br>


Les caractéristique de la liaison série du TVI-920C peuvent être
paramétrées grâce à des petits interrupteurs groupés dans deux
emplacements. Le premier emplacement situé à l\'intérieur du terminal
permet de fixer la vitesse de la transmission et le second groupe situé
sur la face arrière de l'appareil permet de fixer les autres paramètres
de la communication. Les caractéristiques générales sont les suivantes :

-   7 Bits de données
-   1 Bit de parité paire
-   1 Bit de stop
-   9600 Bauds
-   Communication asynchrone

### 5. L'ordinateur central
----------------------------------

#### Description

L'ordinateur central qui est au coeur de toute l'infrastructure du
projet est en fait un ordinateur de bureau tout ce qu'il y a de plus
classique. Il s'agit en fait du recyclage d'un ordinateur plutôt âgé .
Cet ordinateur fonctionne sous Linux Fedora[^19].

#### Installation et configuration

<img src="images/ordi.JPG" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*16. Illustration: Vue des prises séries de
l\'ordinateur*<br>

Avant son installation l'ordinateur a été testé à
l'aide de memtest[^20] puis ensuite en démarrant une distribution Linux
a partir d'un live CD un test du disque dur a été effectué par la
commande suivante :

```
badblocks -wvs /dev/sda
```

Une fois que la machine fut déclarée comme fiable, deux cartes
d'extension permettant de rajouter chacune deux liaisons séries furent
installées. La machine fut ensuite installée, paramétrée, le système
d'exploitation et les logiciel ont été mis à jour, Java fut installé et
un problème fut découvert lorsque en comptant les liaisons séries
disponibles sur la machine, il n'en fut trouvées que quatre. La solution
est de passer le paramètre suivant au noyau lors de son démarrage par
GRUB[^21] pour pouvoir disposer de 10 liaisons séries au maximum :

```
8250.nr\_uarts=10
```

Après il a fallu identifier quelle prise correspondait à quel
identifiant. Et les étiqueter pour éviter d'avoir des problèmes avec le
programme développé pour répondre au cahier des charges.

--------------------------------------------------------

[^4]: Sun Microsystems était un éditeur de logiciels et constructeur
    d'ordinateurs américain créé en 1982 et racheté par Oracle en 2009.
    Sun a notamment développé Java.

[^5]: Il s'agit d'un langage de programmation informatique orienté objet
    créé par Patrick Naughton et James Gosling et dévoilé le 23 mai
    1995. La quasi-totalité de la programmation réalisé dans ce stage
    l'a été dans ce langage.

[^6]: Le C est un langage de programmation impératif issu de la
    programmation système inventé au début des années 1970 dans les
    laboratoire Bell pour écrire UNIX

[^7]: Le C++ est une évolution du langage C permettant la programmation
    sous de multiples paradigmes notamment la programmation orientée
    objet

[^8]: Le PHP (Hypertext Preprocessor) est un langage de programmation
    libre principalement utilisé pour produire des pages Web dynamiques.

[^9]: Le HTML (HyperText Markup Language) est un langage de description
    principalement utilisé pour construire des pages Web.

[^10]: JavaDoc est un outil permettant de créer une documentation d'API
    en format HTML depuis les commentaires présents dans un code source
    Java.

[^11]: CCITT : Comité consultatif International Téléphonique et
    Télégraphique

[^12]: Équipement Terminal de Traitement de Données

[^13]: Data Terminal Equipement

[^14]: Équipement Terminal de Circuit de Données

[^15]: Data Circuit-terminating Equipement

[^16]: MIDI : Musical Instrument Digital Interface est un protocole qui
    permet de connecter plusieurs instruments musicaux ensemble.

[^17]: Le vidéotex est un service de télécommunications permettant
    l'envoi de pages composées de textes et de graphismes simples à un
    utilisateur via une ligne téléphonique.

[^18]: TeleVideo souvent abrégé TVI est une entreprise américaine créée
    en 1979 par K. Philip Hwang qui était spécialisée dans la
    fabrication de terminaux.

[^19]: Fedora est la version gratuite de Red-Hat

[^20]: memtest est un outil permettant de tester la stabilité d'un
    ordinateur en effectuant des tests de lectures, écritures sur la
    mémoire vive de l'ordinateur.

[^21]: GRand Unified Bootloader est un des programmes d'amorçage utilisé
    par les micro-ordinateur.