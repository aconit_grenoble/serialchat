# Introduction


## 1. Association Aconit 
------------------------------

L'Association pour un CONservatoire de l'Informatique et de la
Télématique[^1] (ACONIT) est une association loi 1901 qui a été crée le
24 janvier 1985 et dont le but est « de créer les structures permettant
l\'étude et l'illustration de l\'évolution de l'informatique au sens
large en faisant revivre son histoire passée et en suivant ses
développements futurs »[^2]. Plus généralement l\'association ACONIT
milite pour la création d'un conservatoire du patrimoine informatique en
France et plus particulièrement dans la région Grenobloise, car comme
nous le savons tous, le bassin Grenoblois est à l'origine de nombreuses
avancées scientifiques et technologiques grâce à ses universités qui
sont reconnues dans le monde entier et grâce à ses nombreuses
entreprises de hautes technologies.

Le Conservatoire National des Arts et Métiers (CNAM) est un des
partenaires majeurs de l'association ACONIT, car depuis 2005
l'association s'est vue confier une mission locale de sauvegarde,
d'inventaire et de mise en valeur du patrimoine scientifique et
technique contemporain, la mission PASTEC[^3]. Dans le cadre de cette
mission l'association effectue d'une part l'inventaire et la
valorisation des collections informatiques existantes et effectue
d'autre part un travail important de repérage et d'inventaire sur les
instruments scientifiques présents dans différents établissements
d'enseignement supérieur et de recherche de l\'académie de Grenoble.

De par la taille et l\'importance historique de sa collection de
matériel informatique, scientifique et technique l'association ACONIT
dispose d'une des collections les plus intéressantes et les plus
complètes présentes sur le sol Français. Cette collection peut se
visiter sur rendez-vous ou lors des journées portes ouvertes tous les
mercredi et durant les journées du patrimoine.

## 2. Objectif du stage
------------------------------

L'objectif du stage était de doter l'association ACONIT d'un système
permettant de faire des démonstrations avec différents types de
terminaux utilisés en informatique avant la généralisation des terminaux
clavier/écran/souris actuels. De plus il m'a été demandé de pouvoir
connecter plusieurs clients réseaux sur le serveur.

<img src="images/enssemble_total.JPG" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*1. Illustration: Exemple de connexion de
périphérique*<br>
<img src="images/diagramme_echanges.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="500" /><br>
*2. Illustration: Diagramme des échanges entres les
périphériques*<br>

------------------------------

[^1]: La télématique est l'ensemble des applications associant les
    télécommunications et l'informatique. Il s'agit d'un terme français
    qui est apparu avec la création du Minitel dans les années 1970.

[^2]: Article 1 des status

[^3]: PASTEC : mission nationale de sauvegarde du patrimoine
    scientifique et technique contemporain. Crée le 25 février 2004 par
    le CNAM [www.pastec.fr](../www.pastec.fr)