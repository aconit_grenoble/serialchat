# L'aspect Programmation


## 1. Description
-------------------------

Le but du programme qui a été développé dans le cadre de ce stage est
d\'inter-connecter tous les périphériques entre eux via un ordinateur
central tout en pouvant connecter en plus un ou plusieurs client sur le
réseau comme illustré dans la[ figure
1](#exemple_de_connexion_de_peripherique|graphic) ainsi que dans la
[figure2](#Objet1|ole). Ce programme a été intégralement réalisé en Java
a l'aide de l'IDE NetBeans.

## 2. Un message unique ?
---------------------------------

<img src="images/myMessage.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="600" /><br>
*17. Illustration: Représentation UML de l\'objet
myMessage*<br>


Pour hiérarchiser les communications entre les différents clients, il
fallait définir de quoi était composé un message à transmettre. Un objet
de type « myMessage » a donc été créé. Cet objet[^22] contient donc les
informations suivantes :

-   l'identifiant de l\'émetteur sur deux chiffres
-   l\'identifiant du destinataire sur deux chiffres
-   Un code permettant d'indiquer quel est le but de ce message
-   Le message à proprement parler sous forme d'une chaîne de caractères

Par exemple pour créer un nouveau message contenant un message a
transmettre depuis le programme central vers un des drivers, on
utiliserait l'appel de création d'objet suivant :

```
**new** myMessage(EmeID, DestID, 0001, \"message à transmettre\");
```

## 3. Un driver générique ?
-----------------------------------

Une question s'est posée dans les débuts de la programmation. Comment
unifier les communications vers des périphériques aussi variés ? Un
objet de type « driverGeneric » a donc été écrit. Cet objet permet de
définir les principales méthodes qui seront utilisées pour pouvoir
communiquer avec les différents terminaux RS-232C qui sont connectés au
serveur. Pour pouvoir écrire un driver spécifique à chaque terminal on
effectue un polymorphisme[^23] par sous-typage de l'objet
« driverGeneric » en ne ré-écrivant que certaines méthodes contenant des
codes particuliers à chaque terminal comme les retours à la ligne, les
effacement d\'écran, etc\...

Tous les périphériques étant inscrits dans un tableau de type
driverGeneric nommé *periph* pour pouvoir transmettre un message à un
périphérique on fait :

```
periph\[ID\].append(****new**** myMessage(99, recus.getEmeId(), 0004,
\"Impossible de s\'envoyer un message a soi même\"));
```

<img src="images/driverGeneric.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="600" /><br>
*18. Illustration: Représentation UML des différents objets nécessaire
au communication entre les terminaux et le
serveur*<br>

Dans l'exemple ci-dessus on transmet un message d'erreur
du programme central vers un des périphériques pour l'informer qu'il n'a
pas l'autorisation de s'envoyer un message à lui même.

## 4. Communication via le réseau
-----------------------------------------

Dans les dernières semaines, il m'a été demandé d'ajouter la possibilité
de connecter un ou plusieurs ordinateurs classiques via un client réseau
pour pouvoir étendre le nombre de personnes pouvant communiquer entre
eux.

### Le serveur réseau.

Le serveur est un serveur réseau utilisant le protocole TCP/IP[^24] qui
est lancé sur le serveur lors du démarrage en écoute sur le port 1994.
ce serveur attend la connexion de nouveau client avant de lancer un
processus léger qui va se charger des interactions entre le client qui
vient de se connecter et le serveur.

<img src="images/serveur_reseau_uml.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="600" /><br>
*19. Illustration: Diagramme UML du serveur
TCP*<br>

### Le client réseau

Lors du démarrage du programme une fenêtre nous demande si l'on veut
être un client ou un serveur. Si on choisit d'être un client, une
fenêtre graphique s'ouvre. Cette fenêtre est l'interface principale de
gestion du client.

<img src="images/vue_de_l'interface_du_client1.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="600" /><br>
*20 Illustration: Vue de la fenêtre de gestion du client
réseau*<br>

<img src="images/client_reseau_uml.png" alt="Kitten"
	title="1. Illustration: Exemple de connexion de
périphérique" width="600" /><br>
*21. Illustration: Diagramme UML du client
réseau*<br>


## 5. Comment gérer le lancement automatique du logiciel ?
------------------------------------------------------------------

Pour pouvoir lancer automatiquement le logiciel qui a été développé un
script « init » a été écrit. Ce dernier permet le lancement au démarrage
du logiciel. Il faut donc spécifier en commentaire au début du script le
bloc suivant en conservant la même structure.


**\#\#\# BEGIN INIT INFO**<br>
**\# Provides: serialChat**<br>
**\# Required-Start: \$local\_fs \$remote\_fs**<br>
**\# Required-Stop: \$local\_fs \$remote\_fs**<br>
**\# Should-Start: \$network**<br>
**\# Should-Stop: \$network**<br>
**\# Default-Start: 2 3 4 5**<br>
**\# Default-Stop: 0 1 6**<br>
**\# Short-Description: Serveur SerialChat**<br>
**\# Description: Lance le client serialChat**<br>
**\#\#\# END INIT INFO**<br>



---------------------

[^22]: Un objet en informatique est un ensemble de logiciel qui comporte
    des variables et des méthodes. Les méthodes étant semblable à des
    fonctions dans un langage procédural.

[^23]: Le polymorphisme permet à un même code d\'être utilisé avec
    plusieurs types ce qui permet d'écrire du code plus abstrait et
    général.

[^24]: TCP : Transmission Control Protocol