package org.aconit.net;

import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Permet de gerer l'emission vers les clients.
 * @author Georges Schwing
 */
public class emit implements Runnable {

    private final PrintWriter out; //permet de gerer la sorite vers le peripherique
    private String message; //contient le message qui vas etre envoyer vers les peripheriques
    private Scanner sc; //contient le scanner qui est utiliser pour la lecture de ce qui est rentrer via le clavier

    /**
     * Permet de cree un nouveau Objet pour ecrire sur les peripheriques de sortie.
     * @param pOut Contient le flux pour ecrire sur le reseau.
     */
    public emit(PrintWriter pOut) {
        out = pOut;
    }

    public void run() {
        //pour l'instant le chat ce fait via le clavier
        sc = new Scanner(System.in);
        while (true) {
            message = sc.nextLine();
            out.println(message);
            out.flush();
        }
    }
}
