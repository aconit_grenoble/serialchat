package org.aconit.net;

import java.io.BufferedReader;
import java.io.IOException;
import org.aconit.net.client.recmitClt;
import org.aconit.net.server.recemitserv;

/**
 * Permet de gerer la reception des messages envoyer par les client.
 * @author Georges Schwing
 */
public class rec implements Runnable {

    private final BufferedReader br; //contient le BufferedReader qui est utilise pour la lecture des infos que les clients transmette
    private String message; //contient le message qui est entendue et le message qui sera envoye
    private recemitserv topServ; //conitient le l'appelant de type serveur
    private recmitClt topClt; //contient l'appelant de type client
    private final String type; //contient le type d'appelant

    /**
     * Permet de cre objet qui est utiliser pour lire ce que les clients envoi sur le serveur.
     * @param pBr Contient le BufferedReader qui est utilise pour la lecture des messages.
     */
    public rec(BufferedReader pBr) {
        br = pBr;
        type = "rien";
    }

    /**
     * Permet de cre objet qui est utiliser pour lire ce que les clients envoi sur le serveur.
     * @param pBr Contient le BufferedReader qui est utilise pour la lecture des messages.
     * @param pTop Contient l'object qui a appeller de type client.
     */
    public rec(BufferedReader pBr, recmitClt pTop) {
        type = "client";
        br = pBr;
        topClt = pTop;
    }

    /**
     * Permet de cre objet qui est utiliser pour lire ce que les clients envoi sur le serveur.
     * @param pBr Contient le BufferedReader qui est utilise pour la lecture des messages.
     * @param pTop Contient l'object qui a appeller de type serveur.
     */
    public rec(BufferedReader pBr, recemitserv pTop) {
        type = "server";
        br = pBr;
        topServ = pTop;
    }

    public void run() {
        while (true) {
            try {
                //pour l'instant les messages recus sont simplement afficher sur la console
                message = br.readLine();
                if (type.equals("rien")) {
                    //rien de special
                    System.out.println(message);
                }
                if (type.equals("server")) { //a ete cre par un serveur
                    topServ.getMess(message);
                }
                if (type.equals("client")) {
                    topClt.getMess(message);
                }
            } catch (IOException e) {
                e.printStackTrace(System.err);
            }
        }
    }
}
