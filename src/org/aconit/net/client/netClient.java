package org.aconit.net.client;

import java.net.Socket;
import java.net.UnknownHostException;
import java.io.IOException;
import org.aconit.gui.ChatClientGUI;

/**
 * Permet de lancer le Client pour ce connecter au serveur (TCP).
 * @author Georges Schwing
 */
public class netClient {

    private Socket mySocket; //contient le socket qui sera utiliser pour le client.
    private Thread t1; //contient le Thread qui sera utiliser a proprement parler pour le client
    private boolean isConnect = false; //indique si on est connecter a un serveur
    private String NickName; //indique le pseudo sous lequel on est connecter
    private final ChatClientGUI top; //contient l'interfaceGUI qui a appeller le client reseau
    private recmitClt Down; // contient le client serveur a proprement parler

    /**
     * Constructeur par default
     * @param pTop processus appelant.
     */
    public netClient(ChatClientGUI pTop) {
        top = pTop;
    }

    /**
     * Ferme la connexion au serveur. 
     */
    public void disconnect() {
        if (isConnect) {
            Down.stop();
            isConnect = false;
        }
    }

    /**
     * Renvoie l'état de la connection au serveur.
     * @return true si on est connecté au serveur.
     */
    public boolean isConnected() {
        return isConnect;
    }

    /**
     * Etablit la connexion au serveur. <br>
     * Si on était déja connecté, ferme la connexion avant de se connecté à nouveau. <br>
     * Si la connexion échoue, {@link isConnected()} renvoi false.
     * @param pHost adresse IP ou nom du serveur à contacter
     * @param pPort port TCP d'écoute du serveur
     * @param NickName pseudo sous lequel on s'identifie.
     */
    public void connect(String pHost, int pPort, String NickName) {
        try {
            //demande de connexion
            mySocket = new Socket(pHost, pPort);
            //le reste n'est executer qu si la connexion a bien eu lieu
            Down = new recmitClt(mySocket, this);
            t1 = new Thread(Down);
            t1.start();
            isConnect = true;
        } catch (UnknownHostException e) {
            System.err.println("impossible de ce connecter au serveur");
            isConnect = false;
        } catch (IOException e) {
            System.err.println("Aucun serveur a l'ecoute du port 1994");
            isConnect = false;
        }
    }

    /**
     * Envoie un message au serveur. <br>
     * N'a aucun effet si on n'est pas connecté.
     * @param pStr message à envoyer
     */
    public void send(String pStr) {
        if (isConnect) {
            Down.setMess(pStr);
        }
    }

    /**
     * Permet de transmettre un message a l'interface graphique.
     * @param pMess Message a transmettre.
     */
    public void getMess(String pMess) {
        top.append(pMess);
    }
}
