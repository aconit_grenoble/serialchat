package org.aconit.net.client;

import java.util.Scanner;
import java.net.Socket;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import org.aconit.net.rec;

/**
 * Permet de gerer le client reseau a proprement parler.
 * @author Georges Schwing
 */
public class recmitClt implements Runnable {

    private final Socket mySocket; //contient le socket qui est utiliser par le client
    private PrintWriter out; //permet d'ecrire sur le socket
    private BufferedReader br; //permet de lire ce qui passe sur le socket
    private Scanner sc; //permet de lire les entrees sur le clavier
    private Thread t1, t2; //contient les thread qui serons utiliser pour la lecture et l'ecriture sur le socket
    private final netClient top; //contient le netClient qui a instancier l'objet

    /**
     * Permet de cree un nouveau objet pour la lecture et l'ecriture sur le reseau.
     * @param s Contient le Socket qui est uriliser pour la lecture et l'ecriture.
     * @param pTop Processus appelant.
     */
    public recmitClt(Socket s, netClient pTop) {
        mySocket = s;
        top = pTop;
    }

    public void run() {
        //pour l'instant les entrer et sortie ne ce font que dans la console
        try {
            out = new PrintWriter(mySocket.getOutputStream());
            br = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
            sc = new Scanner(System.in);
            t2 = new Thread(new rec(br, this));
            t2.start();
        } catch (IOException e) {
            System.err.println("Les serveur est deconnecte");
            System.exit(1);
        }
    }

    /**
     * Permet de quitter le client proprement.
     */
    public void stop() {
        out.println("quit");
        out.flush();
        t2.stop();
        out.close();
        try {
            br.close();
        } catch (IOException e) {
            System.err.println("erreur d'entre sortie");
        }
    }

    /**
     * Permet de recuperer un message que l'on vient de recevoir pour ensuite le transmettre vers l'interface graphique.
     * @param pMess Contient le message a transmettre
     */
    public void getMess(String pMess) {
        if (pMess.equals("quitt")) {
            stop();
        }
        top.getMess(pMess);
    }

    /**
     * Permet de transmettre un message sur le reseau vers le serveur distant.
     * @param pMess Le message a transmettre.
     */
    public void setMess(String pMess) {
        out.println(pMess);
        out.flush();
    }
}
