package org.aconit.net.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import org.aconit.util.myMessage;

/**
 * Cette class est charge de d'accorder l'autorisation pour la connexion de
 * nouveau client.
 *
 * @author Georges Schwing
 */
public class allow implements Runnable {

    private final ServerSocket mySocketServer; //contient le SocketServer generer par netServer
    private Socket mySocket; //contient le socket qui est utiliser par l'object
    private Thread t1; //contient le Thread qui est utiliser
    private recemitserv myTrans;//contient l'object qui gere les transmission a proprement parler
    private boolean canDo; //indique si on peut continuer la demande de connexion
    private final netServer topServ; //contient le serveur du dessus
    private final boolean isClt = false; //indique si il y a deja eu des client connecter
    private final recemitserv[] trans = new recemitserv[100]; //contient la liste de tout les peripheriques disponibles pour envoyer un message
    private final boolean[] dispo = new boolean[100]; //permet de savoir si le peripherique site plus haut est disponible

    /**
     * Permet de creer un nouveau object de type allow
     *
     * @param ss Contient le SocketServer qui est generer par netServer
     * @param pTop Contient l'objet createur.
     */
    public allow(ServerSocket ss, netServer pTop) {
        this.mySocketServer = ss;
        topServ = pTop;
        for (int i = 0; i <= dispo.length - 1; i++) {
            dispo[i] = true;
        }
    }

    public void run() {
        try {
            while (true) {
                canDo = false;
                mySocket = mySocketServer.accept(); //lance la connexion quand un client est detecter
                //il faut demander un id
                myMessage message = new myMessage(100, 99, 0005, "Ordinateur2");
                message.setBool(false);
                getMess(message);
            }
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Permet d'ajouter un message vers les clients qui sont present sur le
     * reseau.
     *
     * @param pMess Contient le message qui doit etre transmit
     */
    public void setMess(myMessage pMess) {
        if (pMess.getCode() == 0005) { //que faire si il s'agit d'un nouvelle ID
            dispo[Integer.parseInt(pMess.getMessage())] = false;
            trans[Integer.parseInt(pMess.getMessage())] = new recemitserv(mySocket, Integer.parseInt(pMess.getMessage()), this);
            t1 = new Thread(trans[Integer.parseInt(pMess.getMessage())]);
            t1.start();
        } else {
            if (!dispo[pMess.getDestId()]) {
                trans[pMess.getDestId()].setMess(pMess);
            }
            if (pMess.getDestId() == 0) {
                for (int i = 0; i <= dispo.length - 1; i++) {
                    if (!dispo[i]) { ///que faire si il y a un peripherique auquelle envoyer un message, on l'envoie
                        trans[i].setMess(pMess);
                    }
                }
            }
        }
    }

    /**
     * Permet de transmettre un message vers la couche du dessus.
     *
     * @param pMess Le message a transmettre.
     */
    public void getMess(myMessage pMess) {
        topServ.getMess(pMess);
    }
}
