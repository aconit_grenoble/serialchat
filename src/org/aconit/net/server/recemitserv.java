package org.aconit.net.server;

import java.net.Socket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import org.aconit.net.rec;
import org.aconit.util.myMessage;

/**
 * Contient le serveur en mode Lecture Ecriture dans son ensemble.
 * @author Georges Schwing
 */
public class recemitserv implements Runnable {

    private final Socket mySocket; //contient le socket qui est utilise pour la lecture du flux
    private BufferedReader br; //contient le BufferedReader qui est utilise pour la lecture
    private PrintWriter out; //contient le PrintWriter qui est utilise pour l'ecriture sur le poste client
    private int id = 100; //contient le nom du client
    private Thread t1, t2; //contientles Threads qui seront utilises pour la lecture et l'ecriture
    private allow topAllow; //conmtient le procssus appelant
    private boolean isConnect = false; //indique si il y a un client connecter
    private String[] lstId; //contient la liste des IDs ainsi que les noms de machines associers

    /**
     * Permet de creer un nouveau serveur en mode Lecture Ecriture.
     * @param s Contient le socket qui est utiliser par le serveur
     * @param plog Contient le nom du nouveau client sur le System
     * @param pTop Contient le processus qui a appeler.
     */
    public recemitserv(Socket s, int pId, allow pTop) {
        mySocket = s;
        topAllow = pTop;
        id = pId;
    }

    public void run() {
        try {
            br = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
            out = new PrintWriter(mySocket.getOutputStream());
            t1 = new Thread(new rec(br, this));
            t1.start();
            isConnect = true;
        } catch (IOException e) {
            //est provoquer lorsque un client ce deconecter
        }
    }

    /**
     * Permet d'ajouter un message vers les clients qui sont present sur le reseau.
     * @param pMess Contient le message qui doit etre transmit
     */
    public void setMess(myMessage pMess) {
        switch (pMess.getCode()) {
            case 0001:
                if (pMess.getDestId() == id || pMess.getDestId() == 00) {
                    out.print(pMess.getString(true));
                    out.flush();
                }
                break;
            case 0003:
                out.print("l'ID " + pMess.getDestId() + " n'est pas valide!!!!!!!!!!" + "\n");
                out.flush();
                out.print("");
                out.flush();
                lstId = pMess.getLstId();
                for (int i = 0; i < pMess.getLstId().length; i++) {
                    if (lstId[i] != null) {
                        if (i < 10) {
                            out.print(i + "  : " + lstId[i] + "\n");
                            out.flush();
                        } else {
                            out.print(i + " : " + lstId[i] + "\n");
                            out.flush();
                        }
                    }
                }
                break;
            case 0004:
                out.print("Impossible de s'auto envoyer un messate!!!" + "\n");
                out.flush();
                break;
        }
    }

    /**
     * Permet de recuperer un message cree par un client reseau.
     * @param pMess Contient le message
     */
    public void getMess(String pMess) {
        if (pMess.equals("quit")) { //que faire si le client veut quitter
            myMessage message = new myMessage(id, 99, 0006, "je quitte");
            topAllow.getMess(message);
            isConnect = false;
            t1.stop();
        } else {
            myMessage message;
            switch ((Integer.parseInt(pMess.substring(0, 4)))) {
                case 0000:
                    message = new myMessage(id, Integer.parseInt(pMess.substring(4, 6)), 0000, pMess.substring(6));
                    topAllow.getMess(message);
                    break;
                case 0007:
                    message = new myMessage(id, 99, 0007, pMess.substring(6));
                    message.setBool(false);
                    topAllow.getMess(message);
                    break;
            }
        }
    }
}

