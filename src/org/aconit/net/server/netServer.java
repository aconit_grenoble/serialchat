package org.aconit.net.server;

import java.net.ServerSocket;
import java.util.Observable;
import java.io.IOException;
import org.aconit.util.myMessage;

/**
 * Ceci est la class principal qui permet de faire un nouveau serveur (TCP) sur le reseaux.
 * @author Georges Schwing
 */
public class netServer extends Observable {

    private ServerSocket ss; //contient le socket qui est utilise par le serveur
    private Thread t; //contient le Thread du serveur a proprement parler
    private allow newAllow; //contient le nouveau allow

    /**
     * Permet de lancer un nouveau server sur le reseaux
     */
    public netServer() {
        try {
            ss = new ServerSocket(1994);
            newAllow = new allow(ss, this);
            t = new Thread(newAllow);
            t.start();
        } catch (IOException e) {
            System.err.println("Le port " + ss.getLocalPort() + " est deja utilise !!!!!!");
            System.err.println(ss.getInetAddress().getHostAddress());
        }
    }

    /**
     * Permet de transmettre  un message vers les clients connectes via le reseau IP.
     * @param pmess Contient le message qui doit etre transmit vers les peripheriques.
     */
    public void append(myMessage pmess) {
        newAllow.setMess(pmess);
    }

    /**
     * Permet de faire remonter un message en notifiant les observers.
     * @param pMess Le message a faire notifier;
     */
    public void getMess(myMessage pMess) {
        this.setChanged();
        this.notifyObservers(pMess);
    }
}
