package org.aconit.gui;

import org.aconit.util.myMessage;
import java.awt.Frame;
import java.awt.TextArea;
import java.awt.Button;
import java.awt.TextField;
import java.awt.MenuBar;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.Label;
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Permet de lancer l'interface graphique principal 
 * @author georges
 */
public class interfaceGUI extends Observable {

    //liste des variables utilise dans le gestionnaire graphique
    private final Frame fenetre; //fenetre principale de l'interface graphique
    private final int id; //id du peripherique de lecture ecriture ici l'ordinateur
    private TextArea texte; //zone de texte au millieu de l'ecran
    private Button sendBtn; //Bouton d'envoi situer sur la fenetre principal
    private TextField msgTxtFld; // zone de texte qui est utilise pour envoyer des messages depuis la fenetre principal
    private MenuBar myMenu; //barre de menu de la fenetre principal
    private Menu fichier; //sous menu "fichier" de la fenetre principal
    private MenuItem quitt; //bouton "Quitter" du menu "fichier" servant a fermer la fenetre principal
    private Menu affichage; //menu "Affichage" de la fenetre principal
    private MenuItem lstCom; //Bouton "Liste des ports series" du menu "Affichage"
    private Menu conf; //Menu "Configuration" de la fenetre principal
    private Menu aide; //Menu "?" de la fenetre principal
    private MenuItem aPropos; //Bouton "A propos de SerialChat" du sous menu "?" de la fenetre principal
    private MenuItem ajou; //boucon ajouter un peripherique du menu fichier
    private Panel sud; //Panel contenant la barre d'etat et les envoies de message de la fenetre principal
    private Label msgLbl; //symbole message en bas de la fenetre principal
    private Label barreStatut; //Barre de statut sistuer aux bas de l'ecran
    private String[] lstId; //contient la liste des IDs
    private TextField idTxtFld; //chant de texte permettant de d'enetrer un ID pour le destinataire du message
    private Panel msgPnl; //panel comportant le champ texte et le champ idTxtFld
    private Label idLbl; //champ qavec ecrit Label
    private Pattern patId; //pattern de verification de la validite de l'ID
    private Matcher matchId; //validiter du motif ci dessus, l'id est ok
    private Frame ajouFenetre; //fenetre pour ajouter un peripherique
    private final ClassLoader cl;

    public interfaceGUI(int pId) {
        id = pId;
        cl = this.getClass().getClassLoader();
        fenetre = new Frame("SerialChat Version 1");
        fenetre.setLayout(new BorderLayout()); // application d'un type BorderLayout a la fenetre principal
        initGUI(); //placement des items de la fenetre principal
    }

    /**
     * methode permettant d'ajouter dans la zone de texte les infos transmise par le programme central
     * @param message
     */
    public void append(myMessage message) {
        myMessage recus = (myMessage) message;
        switch (recus.getCode()) {
            case 0001://message recus du programme central vers un programme d'entrer sortie
                lstId = recus.getLstId();
                texte.append(lstId[recus.getEmeId()] + " vers " + lstId[recus.getDestId()] + " : " + recus.getMessage() + "\n");
                break;
            case 0002: //recevoir la liste des Id
                lstId = recus.getLstId();
                new lstCOM(lstId);
                break;
            case 0003: //recevoir le message d'erreur qui dit que l'id de destination est faux
                texte.append("l'ID " + recus.getDestId() + " n'est pas valide!!!!!!!!!!" + "\n");
                idTxtFld.setText("");
                lstId = recus.getLstId();
                for (int i = 0; i < lstId.length; i++) {
                    if (lstId[i] != null) {
                        if (i < 10) {
                            texte.append(i + "  : " + lstId[i] + "\n");
                        } else {
                            texte.append(i + " : " + lstId[i] + "\n");
                        }
                    }
                }
                texte.append("\n");
                break;
            case 0004:
                texte.append("Impossible de s'auto envoyer un message! \n");
                idTxtFld.setText("");
                break;
        }

    }

    /**
     * methode permettant de recevoir des infos de la fenetre d'ajout de peripherique
     * @param pString message transmit par la fenetre d'ajout de peripherique
     */
    public void addPeriph(String pString) {
        System.out.println(pString);
    }

    /**
     * methode send pour pouvoir recevoir un message du programme central SerialChat
     * @param pMessage message qui doit etre transmit vers le programme principal
     */
    public void send(myMessage pMessage) {
        this.setChanged(); //on signal a l'observateur que l'etat a changer
        this.notifyObservers(pMessage); //puis on envoi le message vers SerialChat
    }

    /**
     * methode d'initialisation pour "construire" la fenetre
     */
    public void initGUI() {
        //creation d'une barre de menu
        myMenu = new MenuBar();
        //creation d'un menu "Fichier" ainsi que ses sous menus
        fichier = new Menu("Fichier");
        ajou = new MenuItem("ajouter un peripherique");
        ajou.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent pEv) {
                ajouFenetre = new Frame("Ajout d'un peripherique");
                ajouFenetre.setIconImage(Toolkit.getDefaultToolkit().getImage(cl.getResource("aconit.png")));
                ajouFenetre.setSize(300, 150);
                ajouFenetre.setVisible(true);
                ajouFenetre.addWindowListener(new WindowAdapter() {

                    @Override
                    public void windowClosing(WindowEvent pEv) {
                        ajouFenetre.setVisible(false);
                        addPeriph("tru");
                    }
                });
            }
        });
        fichier.add(ajou);
        quitt = new MenuItem("Quitter");
        quitt.addActionListener(new quittListener());
        fichier.add(quitt);
        myMenu.add(fichier);
        //creation d'un menu "Affichage" ainsi que ses sous menus
        affichage = new Menu("Affichage");
        lstCom = new MenuItem("Liste des ports series");
        lstCom.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent pEv) {
                //que doit on faire lorsque on doit ouvrir la fenetre liste des ports series
                send(new myMessage(id, id, 0002, msgTxtFld.getText()));
            }
        });
        affichage.add(lstCom);
        myMenu.add(this.affichage);
        //creation d'un menu "Configuration" ainsi que ses sous menus
        conf = new Menu("Configuration");
        myMenu.add(conf);
        //creation d'un menu aide "?" ainsi que ses sous menus
        aide = new Menu("?");
        aPropos = new MenuItem("A propos de SerialChat");
        aPropos.addActionListener(new aProposListener());
        aide.add(aPropos);
        myMenu.add(aide);
        fenetre.setMenuBar(myMenu);
        //ajout de la zone de texte servant a l'affichage des communication au millieu de la fenetre
        texte = new TextArea();
        texte.setEnabled(false);
        fenetre.add(texte, BorderLayout.CENTER);
        //ajout d'une barre d'etat et d'envois de message en bas de l'ecran
        sud = new Panel();
        sud.setLayout(new BorderLayout());
        msgLbl = new Label("Message: ", Label.LEFT);
        sud.add(msgLbl, BorderLayout.WEST);
        msgTxtFld = new TextField("", 38);
        msgPnl = new Panel();
        msgPnl.setLayout(new FlowLayout());
        idTxtFld = new TextField(2);
        msgPnl.add(idTxtFld);
        idLbl = new Label("Id ");
        msgPnl.add(idLbl);
        msgPnl.add(msgTxtFld);
        sud.add(msgPnl, BorderLayout.CENTER);
        patId = Pattern.compile("\\s*(\\d{1,2})\\s*"); //definition du ppattern piur la validiter des ID
        msgTxtFld.addActionListener(new ActionListener() { //comment envoyer un message au reste du system

            public void actionPerformed(ActionEvent pEv) {
                matchId = patId.matcher(idTxtFld.getText());
                if (msgTxtFld.getText().equals("")) {
                    //que faire quand le champ de texte est vide: on ne fait rien
                } else {
                    //que faire lorsqu'il y a un truc a transmettre
                    matchId = patId.matcher(idTxtFld.getText());
                    if (matchId.matches()) {
                        send(new myMessage(id, Integer.parseInt(idTxtFld.getText()), 0000, msgTxtFld.getText()));
                        msgTxtFld.setText("");
                    } else {
                        texte.append("l'ID " + idTxtFld.getText() + " n'est pas u ID valide" + "\n");
                        texte.append("ca doit etre un nombre entre 0 et 99" + "\n");
                        texte.append("se referer a \"Liste des ports series et des ID\" dans l'onglet affichage" + "\n");
                        idTxtFld.setText("");
                    }
                }
            }
        });
        sendBtn = new Button("Envoi");
        sud.add(sendBtn, BorderLayout.EAST);
        sendBtn.addActionListener(new ActionListener() { //comment envoyer un message au reste du system

            public void actionPerformed(ActionEvent pEv) {
                if (msgTxtFld.getText().equals("")) {
                    //que faire quand le champ de texte est vide: on ne fait rien
                } else {
                    //que faire lorsqu'il y a un truc a transmettre
                    matchId = patId.matcher(idTxtFld.getText());
                    if (matchId.matches()) {
                        send(new myMessage(id, Integer.parseInt(idTxtFld.getText()), 0000, msgTxtFld.getText()));
                        msgTxtFld.setText("");
                    } else {
                        texte.append("l'ID " + idTxtFld.getText() + " n'est pas u ID valide" + "\n");
                        texte.append("ca doit etre un nombre entre 0 et 99" + "\n");
                        texte.append("se referer a \"Liste des ports series et des ID\" dans l'onglet affichage" + "\n");
                        idTxtFld.setText("");
                    }
                }
            }
        });
        barreStatut = new Label("SerialChat est lance et parfaitement fonctionel", Label.LEFT);
        barreStatut.setBackground(Color.LIGHT_GRAY);
        sud.add(barreStatut, BorderLayout.SOUTH);
        fenetre.add(sud, BorderLayout.SOUTH);
        //definition des parametre de la fenetre principal
        fenetre.setSize(520, 520);
        fenetre.setIconImage(Toolkit.getDefaultToolkit().getImage(cl.getResource("aconit.png")));
        fenetre.setVisible(true);
        fenetre.addWindowListener(new fenetreListener());
    }
}

//creation d'une classe pour les actions effectuer sur le bouton "Quitter" du menu "Fichier" de la fenetre Principal
class quittListener implements ActionListener {

    public void actionPerformed(ActionEvent pEv) {
        //inserer ici ce qui doit etre fait pour quitter le programme
        System.exit(0);
    }
}

//creation d'une classe pour les actions sur la croix de la fenetre principal
class fenetreListener extends WindowAdapter {

    @Override
    public void windowClosing(WindowEvent pEv) {
        //insere ici ce qui doit etre fait pour quitter le programme
        System.exit(0);
    }
}

//creation d'une classe pour l'action sur le bouton "A propos" du menu "?" de la fenetre principal
class aProposListener implements ActionListener {

    private Frame fenetre; //fenetre ouverte grace au bouton "A propos" du menu "?" de la fenetre principal
    private Label texte; //Texte ecris dans la fenetre "A propos"

    public void actionPerformed(ActionEvent pEv) {
        ClassLoader cl = this.getClass().getClassLoader();
        fenetre = new Frame("A propos");
        //ajouter ici le contenu de la fenetre "A propos"
        fenetre.setIconImage(Toolkit.getDefaultToolkit().getImage(cl.getResource("aconit.png")));
        texte = new Label("www.aconit.org");
        fenetre.add(texte);
        fenetre.setSize(200, 50);
        fenetre.setVisible(true);
        fenetre.addWindowListener(new WindowAdapter() { //gestion de la fermeture de la fenetre "A propos"

            @Override
            public void windowClosing(WindowEvent pEv) {
                fenetre.setVisible(false);
            }
        });
    }
}

