package org.aconit.gui;

import java.awt.Toolkit;
import java.awt.Frame;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.Button;
import java.awt.Label;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import org.aconit.net.client.netClient;
import org.aconit.util.osType;
import org.aconit.util.myBfReader;
import org.aconit.driver.base.lectureThread;

/**
 * Cet Objet est l'interface graphique permettant de controler un client reseau.
 *
 * @author Georges Schwing
 */
public class ChatClientGUI extends Frame {

    //liste des variables utilise pour la class
    Frame fenetre = new Frame();
    private TextArea logTxtArea; //contient les message recus et le message envoyer
    private TextField serverTxtFld; //contient les infos sur le serveur
    private TextField nickTxtFld; //contient le pseudo
    private TextField msgTxtFld; //contient le message a proprement parler
    private Button connectionBtn; //bouton permettant la connexion
    private Button sendBtn; //bouton permettant l'envoi du message
    private Label serverLbl; //contient le message "serveur (IP:port)"
    private Label nickLbl; //contient le message "pseudo"
    private Label msgLbl; //contient le message "message:"
    private final netClient myChat; //contient le Chat reseau a proprement parler
    private TextField idTxtFld; //contient l'id qui est necessaire pour cree un message
    private Label idLbl; //contient le message "ID:"
    private final Pattern patId; //contient le Pattern qui est utiliser pour verifier que l'id est valable
    private Matcher matchId; //contient le matcher utiliser pour la verification de la validite de l'id
    private lectureThread lecture; //contient le thread de lecture du fichier
    private boolean isWin; //indique si il s'agit d'un client microsoft

    /**
     * Constructeur par defaut.
     */
    public ChatClientGUI() {
        super("Chat client");
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getClassLoader().getResource("aconit.png")));
        this.initGUI();
        this.initEvent();
        patId = Pattern.compile("\\s*(\\d{1,2})\\s*");
        myChat = new netClient(this);

        //mes ajouts de codes pour la gestion du tactile
        osType typeOs = new osType();
        if (typeOs.isWindows()) { //verification du type d'os qui doit etre egal a windows.
            File fichier = new File("C:\\Reco\\result\\OutputResult.txt"); //C'est ICI QUE L'on definit le chemin d'accer au fichier !!!!!!!!!!!!!!!!!!!!!11111 attention peut etre probleme
            //verification de la presence du fichier
            if (fichier.exists()) {
                //si sa existe on lance la lecture du fichier
                lecture = new lectureThread(fichier.getPath(), this);
                isWin = true;
                logTxtArea.append("le fichier : " + fichier.getPath() + "vas etre lu lors d'un envoi de message");
                //Thread l1 = new Thread(lecture);
                //l1.start();
            } else {
                //sinon on affiche sur la zone de texte que le fichier n'est pas la
                logTxtArea.append("le fichier : " + fichier.getPath() + " n'existe pas");
                isWin = false;
            }
        }
    }

    /**
     * Permet d'ajouter un message sur l'interface graphique.
     *
     * @param pMess Le message qui doit etre traiter puis affiche.
     */
    public void append(String pMess) {
        logTxtArea.append(pMess + "\n");
    }

    /**
     * Permet d'executer toutes les actions necessaire au l'initalisation de
     * l'interface graphique GUI.
     */
    void initGUI() {
        // instantiation des composants LAbel, Button, TextField et TextArea
        this.logTxtArea = new TextArea(" Vous n'etes pas connecte a un serveur...");
        this.serverTxtFld = new TextField("192.168.0.3:1994");
        this.nickTxtFld = new TextField("Ordinateur");
        this.msgTxtFld = new TextField("Bonjour!");
        this.connectionBtn = new Button("Connexion");
        this.sendBtn = new Button("Envoi");
        this.serverLbl = new Label("Serveur(IP:port) :", Label.RIGHT);
        this.nickLbl = new Label("pseudo :", Label.RIGHT);
        this.msgLbl = new Label("Message :", Label.RIGHT);
        this.idTxtFld = new TextField("00", 2);
        this.idLbl = new Label("ID :", Label.RIGHT);

        // mise en disable des composant inutile avant la connection.
        this.logTxtArea.setEnabled(false);
        this.msgTxtFld.setEnabled(false);
        this.sendBtn.setEnabled(false);

        // Placement des composants dans des conteneurs
        this.setLayout(new BorderLayout());
        this.setBackground(Color.LIGHT_GRAY);
        Panel panel11 = new Panel(new GridLayout(2, 1, 5, 5));
        Panel panel12 = new Panel(new GridLayout(2, 1, 5, 5));
        Panel panel13 = new Panel(new BorderLayout(5, 5));
        Panel panel1 = new Panel(new BorderLayout(5, 5));
        panel1.setBackground(new Color(255, 255, 204));
        Panel panel2 = new Panel(new BorderLayout(5, 5));
        Panel panel21 = new Panel(new BorderLayout(5, 5));
        Panel panel22 = new Panel(new GridLayout(1, 2, 5, 5));
        panel11.add(this.serverLbl);
        panel11.add(this.nickLbl);
        panel12.add(this.serverTxtFld);
        panel12.add(this.nickTxtFld);
        panel13.add(this.connectionBtn, BorderLayout.SOUTH);
        panel1.add(panel11, BorderLayout.WEST);
        panel1.add(panel12, BorderLayout.CENTER);
        panel1.add(panel13, BorderLayout.EAST);
        panel2.add(this.msgLbl, BorderLayout.WEST);
        panel22.add(this.idLbl);
        panel22.add(this.idTxtFld);
        panel21.add(panel22, BorderLayout.EAST);
        panel21.add(this.msgTxtFld, BorderLayout.CENTER);
        panel2.add(panel21, BorderLayout.CENTER);
        panel2.add(this.sendBtn, BorderLayout.EAST);
        this.add(panel1, BorderLayout.NORTH);
        this.add(this.logTxtArea, BorderLayout.CENTER);
        this.add(panel2, BorderLayout.SOUTH);

        //affichage de la fenetre
        this.pack();
        this.setVisible(true);
    }

    // il faut griser les fenetre et changer le nom de connxionBtn
    /**
     * Permet de griser les fenetres et changer le nom du bouton connexion en
     * deconexion.
     *
     * @param param true si on veux griser, false sinon
     */
    void grisee(boolean param) {
        if (param == true) {
            connectionBtn.setLabel("Deconexion");
        } else {
            connectionBtn.setLabel("Connexion");
        }
        serverTxtFld.setEnabled(!param);
        nickTxtFld.setEnabled(!param);
        logTxtArea.setEnabled(param);
        msgTxtFld.setEnabled(param);
        idTxtFld.setEnabled(param);
        sendBtn.setEnabled(param);
        pack();
    }

    /**
     * Gestion des evenements (relier les objets sources d'evenemtents aux
     * objets ecouteur)
     */
    void initEvent() {
        // Fermeture de la fenetre
        this.addWindowListener(
                new WindowAdapter() {

                    @Override
                    public void windowClosing(WindowEvent e) {
                        myChat.disconnect();
                        System.exit(0);
                    }
                });

        // Bouton connectionBtn et gestion de validite de l'adresse et du Nick
        this.connectionBtn.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        Pattern patNick = Pattern.compile("\\s*(\\w{4,12})\\s*");
                        Pattern patAddr = Pattern.compile("\\s*((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))" + ":(6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]|[1-5][0-9]{4}|[0-9]{1,4})\\s*");
                        Matcher matchAddr = patAddr.matcher(serverTxtFld.getText());
                        Matcher matchNick = patNick.matcher(nickTxtFld.getText());
                        if (matchAddr.matches() && matchNick.matches()) {
                            if (myChat.isConnected() == false) {
                                myChat.connect(matchAddr.group(1), Integer.parseInt(matchAddr.group(2), 10), nickTxtFld.getText());
                                if (myChat.isConnected() == true) {
                                    logTxtArea.append("\n" + "Connexion reussie sur le serveur " + serverTxtFld.getText() + " sous le pseudo " + nickTxtFld.getText() + "\n");
                                    grisee(true);
                                    chdNick();
                                } else {
                                    logTxtArea.append("\n" + "Connexion echoue sur le serveur  " + serverTxtFld.getText());
                                }
                            } else {
                                myChat.disconnect();
                                if (myChat.isConnected() == false) {
                                    logTxtArea.append("\n" + "Deconnexion au serveur " + serverTxtFld.getText() + " reussie");
                                    grisee(false);
                                } else {
                                    logTxtArea.append("\n" + "Deconnexion au serveur " + serverTxtFld.getText() + " echoue");
                                }
                            }
                        } else {
                            if (matchAddr.matches() == false && matchNick.matches() == false) {
                                Label erreurPseudo = new Label("Le champ de pseudo est incorect");
                                Label erreurAddr = new Label("L'adresse et le port rentre ne sont pas valides ");
                                Panel panelErreur = new Panel(new GridLayout(2, 1, 5, 5));
                                panelErreur.add(erreurAddr);
                                panelErreur.add(erreurPseudo);
                                fenetre.add(panelErreur);
                            } else {
                                Label erreurTxt = new Label();
                                if (matchAddr.matches() == false) {
                                    erreurTxt = new Label(" le champ d'adresse et de port n'est pas sous la forme ip:port ");
                                }
                                if (matchNick.matches() == false) {
                                    erreurTxt = new Label(" Le champ de pseudo est incorect ");
                                }
                                fenetre.add(erreurTxt);
                                fenetre.setTitle(" Erreur !!! ");
                                fenetre.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getClassLoader().getResource("aconit.png")));
                            }
                            fenetre.pack();
                            fenetre.setVisible(true);
                        }
                    }
                });

// Bouton D'envoi
        this.sendBtn.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        send();
                    }
                });

        // Champ de saisie msgTxtFld
        this.msgTxtFld.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        send();
                    }
                });

        // fermeture de la fenetre d'erreur
        fenetre.addWindowListener(
                new WindowAdapter() {

                    @Override
                    public void windowClosing(WindowEvent e) {
                        fenetre.setVisible(false);
                    }
                });
    }

    /**
     * Permet d'effectuer un envoi vers le serveur reseau distant.
     */
    public void send() {
        String line = new String();
        try {
            File f = new File("C:\\Reco\\result\\OutputResult.txt");
            FileReader fr = new FileReader(f);
            myBfReader br = new myBfReader(fr);
            try {
                while (line != null) {
                    System.out.println(line);
                    line = br.readLine();
                }

                br.close();
                fr.close();
            } catch (IOException exception) {
                System.out.println("Erreur lors de la lecture : " + exception.getMessage());
            }
        } catch (FileNotFoundException exception) {
            System.out.println("Le fichier n'a pas été trouvé");
        }

        matchId = patId.matcher(idTxtFld.getText());
        if (line.length() != 0) {
            if (matchId.matches()) { //que faire si il y a un message et que l'ID inscrit est valide
                if (Integer.parseInt(idTxtFld.getText()) < 10) {
                    myChat.send("0000" + "0" + Integer.parseInt(idTxtFld.getText()) + line);
                } else {
                    myChat.send("0000" + Integer.parseInt(idTxtFld.getText()) + line);
                }
                msgTxtFld.setText("");
            } else { //que faire si l'id n'est pas valide
                logTxtArea.append("l'ID " + idTxtFld.getText() + " n'est pas u ID valide" + "\n");
                logTxtArea.append("ca doit etre un nombre entre 0 et 99" + "\n");
                logTxtArea.append("se referer a \"Liste des ports series et des ID\" dans l'onglet affichage" + "\n");
                idTxtFld.setText("");
            }
        }

        if (msgTxtFld.getText().equals("")) {
        } else {
            matchId = patId.matcher(idTxtFld.getText());
            if (matchId.matches()) { //que faire si il y a un message et que l'ID inscrit est valide
                if (Integer.parseInt(idTxtFld.getText()) < 10) {
                    myChat.send("0000" + "0" + Integer.parseInt(idTxtFld.getText()) + msgTxtFld.getText());
                } else {
                    myChat.send("0000" + Integer.parseInt(idTxtFld.getText()) + msgTxtFld.getText());
                }
                msgTxtFld.setText("");
            } else { //que faire si l'id n'est pas valide
                logTxtArea.append("l'ID " + idTxtFld.getText() + " n'est pas u ID valide" + "\n");
                logTxtArea.append("ca doit etre un nombre entre 0 et 99" + "\n");
                logTxtArea.append("se referer a \"Liste des ports series et des ID\" dans l'onglet affichage" + "\n");
                idTxtFld.setText("");
            }
        }
    }

    /**
     * Permet de changer son pseudo
     */
    public void chdNick() {
        myChat.send("000700" + this.nickTxtFld.getText());
    }

    public void ecoute(String recus) {
        matchId = patId.matcher(idTxtFld.getText());
        if (matchId.matches()) { //que faire si il y a un message et que l'ID inscrit est valide
            if (Integer.parseInt(idTxtFld.getText()) < 10) {
                myChat.send("0000" + "0" + Integer.parseInt(idTxtFld.getText()) + recus);
            } else {
                myChat.send("0000" + Integer.parseInt(idTxtFld.getText()) + recus);
            }
        } else { //que faire si l'id n'est pas valide
            logTxtArea.append("l'ID " + idTxtFld.getText() + " n'est pas u ID valide" + "\n");
            logTxtArea.append("ca doit etre un nombre entre 0 et 99" + "\n");
            logTxtArea.append("se referer a \"Liste des ports series et des ID\" dans l'onglet affichage" + "\n");
            idTxtFld.setText("");
        }
    }
}
