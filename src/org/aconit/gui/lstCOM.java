package org.aconit.gui;

import java.awt.Frame;
import java.awt.TextArea;
import java.awt.Toolkit;

import java.awt.event.*;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * permet de cree la fenetre avec la liste des ports series
 * @author Georges Schwing
 */
public class lstCOM {

    //liste des variables utiliser dans cette classe
    private Frame fenetre; //fenetre ouverte par la classe
    private final TextArea texte; //zone de texte contenant la liste des id

    /**
     * constructeur permettant de creer et d'afficher la fenetre
     * @param pId contient le liste des ID des peripheriques
     */
    public lstCOM(String[] pId) {
        ClassLoader cl = this.getClass().getClassLoader();
        fenetre = new Frame("Liste des liasons series et des ID");
        fenetre.setIconImage(Toolkit.getDefaultToolkit().getImage(cl.getResource("aconit.png")));
        //ajouter ici le contenu de la fenetre "Liste des lisasons series et des ID"
        fenetre.setSize(300, 150);
        texte = new TextArea();
        texte.setEnabled(false);
        for (int i = 0; i < pId.length; i++) {
            if (pId[i] != null) {
                texte.append(i + " : " + pId[i] + "\n");
            }
        }
        fenetre.add(texte);
        fenetre.pack();
        fenetre.setVisible(true);
        fenetre.addWindowListener(new WindowAdapter() { //gestion de la fermeture de la fenetre
            @Override
            public void windowClosing(WindowEvent pEv) {
                fenetre.setVisible(false);
            }
        });
    }
}
