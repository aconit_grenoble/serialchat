package org.aconit.driver;

import org.aconit.driver.base.driverGeneric;

/**
 * permet d'ajouter un nouveau peripherique de type TVI-920C
 * parametre de connexion:
 * <ul><li>9600 Bauds</li>
 * <li>7 bits</li>
 * <li>parite paire</li>
 * <li>type ecran</li></ul>
 * @author Georges Schwing
 */
public final class tvi920c extends driverGeneric {

    /**
     * @param pId contient l'ID du peripherique
     * @param pCom contient la prise serie sur modele /dev/ttyS?
     */
    public tvi920c(int pId, String pCom) {
        super(pId, pCom, 7, 9600, "paire", "ecran");
        cls(); //permet d'efface l'ecran, pour eviter d'avoir avant le redemarage du programme
        ASCIIart();
    }

    @Override
    public void CR() {
        aEcrire.setMessage(new String(new byte[]{0xD})); //code permettant de faire un retour a la ligne CR
    }

    @Override
    public void cls() {
        aEcrire.setMessage(new String(new byte[]{0x1E, 0x1B, 0x79}));
    }
}
