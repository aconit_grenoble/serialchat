package org.aconit.driver.base;

import java.util.Observable;
import org.aconit.util.myMessage;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Driver Generique qui contient la base pour parametrer un peripherique
 * @author Georges Schwing
 */
public class driverGeneric extends Observable {
    //definition des variables utile au programme

    private String com; //contient le port serie utiliser par le peripherique /dev/ttyS
    private int id; //id du peripherique
    private Thread l1; //Thread utiliser pour la lecture
    private lectureThread lecture; //permet de lire ce qui rentre sur le port serie
    protected ecriture aEcrire; //peremet d'ecrire sur le port serie
    private int tps; //contient le nobre d'iteration depuis la derniere emission, par default au demarage on a le droit d'ecrire dou la taille superieur a 600
    protected boolean available; // contient un booleen pour savoir si on a le droit d'ecrire
    protected myMessage[] enAttente = new myMessage[100];//contient la liste de message a ecrire
    protected int resteAEcrire;//contient le nombre de message encore a ecrire
    private Pattern patId; // pattern servant a verifier que l'ID est valide
    private Matcher matchId; //Matcher utiliser pour la verification de la validite de l'ID
    //variables utiles a parametrer la connexion serie
    private int nBits; //nombre de bits utiliser pour la conexion serie
    private int bauds; //nombre de bauds pour la connexion
    private String parite; //pariter utiliser pour la connexion
    private boolean mess; //permet de dire si on veut un message long ou cours
    private boolean retour; //permet de dire si on remplace la ligne qui viens d'etre transmise
    private boolean type; //defini le type de terminal, true pour ecran, false papier.
    private boolean isReal = false; //indique si le peripherique est viable

    /**
     * Permet d'instancier un nouveau peripherique generique
     * @param pId contient l'ID du peripherique
     * @param pCom contient la prise serie qui est utiliser pour la communication /dev/ttyS?
     * @param pBits contient le nombre de bits utiliser pour chaque symbole
     * @param pBauds contient la rapidite de modulation en Bauds
     * @param pParite contient le type de parite
     * @param pType signifie le type de machine
     * <ul><li>ecran       terminal a ecran</li>
     * <li>papier      terminal sur papier</li></ul>
     */
    public driverGeneric(int pId, String pCom, int pBits, int pBauds, String pParite, String pType) {
        id = pId;
        com = pCom;
        nBits = pBits;
        bauds = pBauds;
        parite = pParite;
        if (pType.equals("ecran")) {
            type = true; //terminal de type ecran
            retour = true; //par default on affiche les messages mais pour afficher les messages in faut que ce soit un terminal a ecran
        } else {
            type = false; //terminal de type imprimante
            retour = false; //on desactive donc le retour
        }
        new parametrageCOM(com, bauds, parite, nBits);
        resteAEcrire = 0;
        tps = 666;
        aEcrire = new ecriture(id, com);
        mess = false; //par default les message sont dis "court"
        init();
        lecture = new lectureThread(id, com, this);
        l1 = new Thread(lecture); //ondefinit un Thread a partir de lectureThread
        l1.start(); //puis on le lance
        isReal = true;
    }

    /**
     * Constructeur par default, il instancie un objet de type serialChat mais vide.
     * <br> Il est utiliser pour le cas du serveur IP.
     */
    public driverGeneric() {
    }

    /**
     * methode permettant de recevoir les message transmit par le peripherique
     * <br> De plus les mots clees suivants permette d'effecter des actions a partir des terminaux
     * <br>
     * <table>
     * <tr>
     * <th>mots Cles</th>
     * <th>action</th>
     * </tr>
     * <tr><td>list</td>                             <td>envoi la liste des ID's disponible pour la communication</td></tr>
     * <tr><td>clear</td>                            <td>Permet d'effacer le contenu de l'ecran</td></tr>
     * <tr><td>ascii</td>                            <td>Permet d'afficher le logo de l'association ACONIT sur l'ecran du terminal</td></tr>
     * </table>
     * @param recus contient la chaine de caractere qui vient d'etre lu sur le peripherique
     */
    public void ecoute(String recus) {
        boolean isSend = false;
        recus = recus.trim(); //permet de supprimer les doubles espaces et les espace au debut et a la fin
        if (recus.toLowerCase().equals("clear") || recus.toLowerCase().equals("list") || recus.toLowerCase().equals("ascii") || recus.toLowerCase().equals("long") || recus.toLowerCase().equals("short") || recus.toLowerCase().equals("help") || recus.toLowerCase().equals("retour")) { //gestion des commandes sur le terminal
            if (recus.toLowerCase().equals("clear") && type) {//que faire lorsque l'ecran doit etre videe
                cls();
                vide();
            }
            if (recus.toLowerCase().equals("list")) {//demmande affichage de la liste des ID
                this.setChanged();
                this.notifyObservers(new myMessage(id, id, 0002, null));
            }
            if (recus.toLowerCase().equals("ascii") && type) { //demande affichage logo Aconit en ASCII art
                ASCIIart();
            }
            if (recus.toLowerCase().equals("long")) { //demande passage affichage message de type long
                if (!retour) {
                    aEcrire.setMessage("\n");
                    CR();
                }
                vide();
                aEcrire.setMessage("passage en message long" + "\n");
                CR();
                mess = true;
            }
            if (recus.toLowerCase().equals("short")) { //demande affichage message de type court
                if (!retour) {
                    aEcrire.setMessage("\n");
                    CR();
                }
                vide();
                aEcrire.setMessage("passage en message court" + "\n");
                CR();
                mess = false;
            }
            if (recus.toLowerCase().equals("retour") && type) { //demande d'inversion pour le retour des messages envoyes
                retour = !retour;
                aEcrire.setMessage("\n");
                CR();
            }
            if (recus.toLowerCase().equals("help")) { //dermande affichage liste des commandes
                aEcrire.setMessage("list     Affiche tout les ID disponible" + "\n");
                CR();
                aEcrire.setMessage("long     Affichage des message de long" + "\n");
                CR();
                aEcrire.setMessage("short    Affichge des message de court" + "\n");
                CR();
                if (type) {
                    aEcrire.setMessage("retour   Desactive le retour" + "\n");
                    CR();
                    aEcrire.setMessage("clear    Efface l'ecran" + "\n");
                    CR();
                    aEcrire.setMessage("ascii    Affiche le logo d'aconit" + "\n");
                    CR();
                }
                aEcrire.setMessage("help     Affiche ce message d'aide" + "\n");
                CR();
            }
        } else {
            if (!retour) {
                aEcrire.setMessage("\n");
                CR();
            }
            patId = Pattern.compile("\\s*(\\d{2})\\s*");
            if (recus.length() >= 3 && (matchId = patId.matcher(recus.substring(0, 2))).matches() && !isSend) { //on verfifie que la chaine n'est pas null et que l'ID transmit est sur 2 chiffre
                this.setChanged();
                this.notifyObservers(new myMessage(id, Integer.parseInt(recus.substring(0, 2)), 0000, recus.substring(2).trim()));
                isSend = true;
            }
            patId = Pattern.compile("\\s*(\\d{1})\\s*");
            if (recus.length() >= 2 && (matchId = patId.matcher(recus.substring(0, 1))).matches() && !isSend) { //on verfifie que la chaine n'est pas null et que l'ID transmit est sur 1 chiffre
                this.setChanged();
                this.notifyObservers(new myMessage(id, Integer.parseInt(recus.substring(0, 1)), 0000, recus.substring(1).trim()));
                isSend = true;
            } else {
                if (!isSend) {
                    aEcrire.setMessage("L'ID doit etre en debut de ligne et comporter 2 chiffres \n");
                    CR();
                }
            }
            //verfiation du temps depuis le dernier message recus
            if (available) {
                vide();
            }
        }
    }

    /**
     * mehode permettant de vider le tampon d'ecriture
     */
    public void vide() {
        if (resteAEcrire != 0) {
            for (int i = 0; i
                    < resteAEcrire; i++) {
                aEcrire.setMessage(enAttente[i].getString(mess));
                CR();
            }
            resteAEcrire = 0;
        }
    }

    /**
     * permet de gerer l'affichage de message transmit par le programme principale
     * @param message
     */
    public void append(myMessage message) {
        if (isReal) {
            avant();
            switch (message.getCode()) {
                case 0004: //gerer l'auto envoi d'un message
                    vide();
                    aEcrire.setMessage("Impossible de s'auto envoyer un message \n");
                    CR();
                    break;
                case 0002: //permet d'afficher la liste des ID
                    vide();
                    for (int i = 0; i < message.getLstId().length; i++) {
                        if (message.getLstId()[i] != null) {
                            if (i < 10) {
                                aEcrire.setMessage(i + "  : " + message.getLstId()[i] + "\n");
                                CR();
                            } else {
                                aEcrire.setMessage(i + " : " + message.getLstId()[i] + "\n");
                                CR();
                            }
                        }
                    }
                    break;
                case 0003: //permet de gerer le code erreur ceci est un id non valide
                    if (resteAEcrire == 0) {
                        vide();
                    }
                    aEcrire.setMessage("L'ID " + message.getDestId() + " n'est pas valide !!!!!!!!!!!" + "\n");
                    CR();
                    for (int i = 0; i
                            < message.getLstId().length; i++) {
                        if (message.getLstId()[i] != null) {
                            if (i < 10) {
                                aEcrire.setMessage(i + "  : " + message.getLstId()[i] + "\n");
                                CR();
                            } else {
                                aEcrire.setMessage(i + " : " + message.getLstId()[i] + "\n");
                                CR();
                            }
                        }
                    }
                    aEcrire.setMessage("\n");
                    CR();
                    break;
                default: //permet de gerer les message du peripherique vers l'interface graphique et des autres peripheriques vers le ce peripherique
                    if (available) {
                        if (message.getEmeId() == id) {
                            avant();
                            vide();
                        }
                        if (retour) {
                            aEcrire.setMessage(message.getString(mess));
                            CR();
                        }
                        vide();
                    } else {
                        if (message.getEmeId() == id) {
                            avant();
                            if (retour) {
                                aEcrire.setMessage(message.getString(mess));
                                CR();
                            }
                            vide();
                        } else {
                            enAttente[resteAEcrire] = message;
                            resteAEcrire++;
                        }
                    }
            }
        }
    }

    /**
     * envoi du code utiliser pour faire les retours a la ligne de type CR sur le terminal
     */
    public void CR() {
        //redefinit dans les programmes qui deriver de driverGeneric
    }

    /**
     * permet d'envoyer quelque chose avant d'afficher une ligne sur le terminal
     */
    public void avant() {
        //doit etre  redefinit si utilse pour le peripherique
    }

    /**
     * permet de mettre a jour l'autorisation d'ecrire sur le terminal
     * @param pAuto contient l'autorisation ou non d'emettre
     */
    public void setAuto(boolean pAuto) {
        if (!available && pAuto) {
            if (!retour) {
                aEcrire.setMessage("\n");
                CR();
            } else {
                effLigne();
            }
            available = pAuto;
            vide();
        }
        available = pAuto;
    }

    /**
     * Permet d'effacer l'ecran, doit etre reecrit
     */
    public void cls() {
    }

    /**
     * Permet d'afficher le logo de l'association ACONIT sur le terminal
     */
    public void ASCIIart() {
        if (!retour) {
            aEcrire.setMessage("\n");
            CR();
        }
        if (type) {
            cls();
        }
        aEcrire.setMessage("         .,,,,   ,.      " + "\n");
        CR();
        aEcrire.setMessage("        :;;;;;; ;;;,     " + "\n");
        CR();
        aEcrire.setMessage("        ;;;;;;;   ;,     " + "\n");
        CR();
        aEcrire.setMessage("        ;;;;;;;   ;,     " + "\n");
        CR();
        aEcrire.setMessage("        ;;   ;;   ;,     " + "\n");
        CR();
        aEcrire.setMessage("        ;;   :;   ;,     " + "\n");
        CR();
        aEcrire.setMessage("     :::::::::;   ;,     " + "\n");
        CR();
        aEcrire.setMessage("    ::..:::::;;:,:;,     " + "\n");
        CR();
        aEcrire.setMessage("    ::  :;;;;;;;;;;      " + "\n");
        CR();
        aEcrire.setMessage("           .:            " + "\n");
        CR();
        aEcrire.setMessage("     ::::::::            " + "\n");
        CR();
        aEcrire.setMessage("    :::::::::            " + "\n");
        CR();
        aEcrire.setMessage("    ::::   ::            " + "\n");
        CR();
        aEcrire.setMessage("    :::    ::            " + "\n");
        CR();
        aEcrire.setMessage("    ::::   ::            " + "\n");
        CR();
        aEcrire.setMessage("    :::::::::            " + "\n");
        CR();
        aEcrire.setMessage("     ::::::::            " + "\n");
        CR();
        aEcrire.setMessage("              www.aconit.org" + "\n");
        CR();
    }

    /**
     * Doit etre reecrit si on veut executer du code tout de suite apres la configuration
     */
    public void init() {
    }

    /**
     * Permet de dire si on accepte les retours sur le terminal. par default le retour est desactiver.
     * <br>
     * C'est a dire efface t'on la ligne que l'on viens de transmettre pour la remplacer par une ligne plus claire ou non.
     * <br> On ne peut pas faire sa sur les terminaux qui imprimmes.
     * <ul><li>true     permet d'imprimer la ligne</li>
     * <li>false    permet de ne pas imprimer la ligne</li></ul>
     * @param pRetour boolean utilise pour commander le retour
     */
    public void retour(boolean pRetour) {
    }

    /**
     * Methode permettant d'effacer une ligne sur le terminal, doit etre reecrite pour s'adapter au terminal
     */
    public void effLigne() {
    }
}
