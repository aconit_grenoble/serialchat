package org.aconit.driver.base;

import java.io.FileReader;
import java.io.IOException;
import org.aconit.util.duree;
import org.aconit.util.myBfReader;
import org.aconit.gui.ChatClientGUI;

/**
 * Programme qui permet de lire automatiquement sur la prise serie
 *
 * @author Georges Schwing
 */
public class lectureThread implements Runnable {

    //definition des variables qui seront utilise pour executer cet objet
    private myBfReader br;
    private int id;//definition de l'id de la machine
    private String com; //contient le port com qui est utiliser
    private String ligne; // contient la nouvelle ligne qui est lut sur la prise serie
    private driverGeneric driver; //contient le driver qui a appeller cette class
    private duree compteur; //compteur initialiser pour dir si on peut ecrire ou pas
    private Thread c1; //thread utiliser par le compteur
    private boolean type; //indique quel est le type de class a appelle cet objet, true pour drivergeneric, false pour ChatClientGUI
    private ChatClientGUI gui; //contient le GUI qui a appelle le Thread

    /**
     * Constructeur utilise dans le cas de l'appel par driverGeneric.
     *
     * @param pId ID du peripherique
     * @param pCom prise serie utiliser par le peripherique
     * @param pDriver contient le peripherique qui a instancier ce Thread
     */
    public lectureThread(int pId, String pCom, driverGeneric pDriver) {
        this.type = true;
        id = pId;
        com = pCom;
        ligne = "bonjour";
        driver = pDriver;
        compteur = new duree(driver);
        c1 = new Thread(compteur);
        c1.start();
        try {
            br = new myBfReader(new FileReader(com), this);
        } catch (IOException e) {
            //que faire si le fichier est inaxcible ou introuvable
            System.err.println("impossible d'ouvrir le fichier :" + com);
            System.err.println("vous n'avez peut etre pas les droits");
            System.err.println("le fichier n'existe peut-etre pas");
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    /**
     * Constructeur utilise dans le cas de l'appel par ChatClientGUI.
     *
     * @param pCom fichier generer par l'interface tactile
     * @param clientGUI contient le client qui a appelle ce Thread
     */
    public lectureThread(String pCom, ChatClientGUI clientGUI) {
        this.type = false;
        com = pCom;
        ligne = "bonjour";
        gui = clientGUI;
        try {
            br = new myBfReader(new FileReader(com), this);
        } catch (IOException e) {
            //que faire si le fichier est inaxcible ou introuvable
            System.err.println("impossible d'ouvrir le fichier :" + com);
            System.err.println("vous n'avez peut etre pas les droits");
            System.err.println("le fichier n'existe peut-etre pas");
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    //redefinition de la methode run pour implementer Runnable
    public void run() {
        try {
            while (true) { //la boucle vas contenir une temporisation pour eviter de bloquer le system
                //on lit le fichier ligne par ligne
                while ((ligne = br.readLine()) != null) {
                    //que faire lorsque l'on a une nouvelle ligne
                    if (type == true) {
                        //que faire si il s'agit d'un thread instancie par un driverGeneric
                        compteur.allow();
                        driver.ecoute(ligne);
                    } else {
                        //que faire si il s'agit d'un thread instancie par ChatClientGUI
                        gui.ecoute(ligne);
                    }
                }
                while (!br.ready()) {//et on met une pause pour eviter de plomber le system
                    try {
                        Thread.sleep(100); //cette pause fait 100ms
                    } catch (InterruptedException e) {
                        System.err.println("interruption sur une exception");
                    }
                }
            }
        } catch (IOException e) {
            System.err.println("impossible de lire le fichier " + com);
            System.err.println("vous n'avez pas les droits ?");
            e.printStackTrace(System.err);
            System.exit(1);
        } finally {
            try {
                br.close(); //on ferme le tampon de lecture lorsque le Thread est stopper
            } catch (IOException e) {
                System.err.println("impossible de lire le fichier " + com);
                System.err.println("vous n'avez pas les droits ?");
                e.printStackTrace(System.err);
                System.exit(1);
            }
        }
    }

    /**
     * methode getResultat pour pouvoir obtenir le resultat
     *
     * @return renvoi le messagge qui vient d'etre lu sur le port serie
     */
    public String getResultat() {
        return (ligne);
    }

    /**
     * Permet de gerer la frappe d'un nouveau charactere sur le terminal
     */
    public void newChar() {
        compteur.reset();
    }
}
