package org.aconit.driver.base;

import java.io.IOException;

/**
 * permet de parametrer une liason serie en vus de son utilisation par un peripherique
 * @author Georges Schwing
 */
public class parametrageCOM {

    //definition des variables utiliser pour la configuration de la liason serie
    private String maCommande = "stty";//contient la commande qui sera utiliser pour le parametrage

    /**
     * permet de parametrer un port serie en vus de son utilisation par un peripherique
     * @param pCOM contient le port serie qui est utiliser par le programme /dev/ttyS?
     * @param pBaud contient la rapidite de modulation de la liason en Bauds
     * @param pParite contient le type de parite pour cette liason
     * @param pNbBits contient le nombre de bits par symbole pour cette connexion
     */
    @SuppressWarnings("UnusedAssignment")
    public parametrageCOM(String pCOM, int pBaud, String pParite, int pNbBits) {
        Runtime run = Runtime.getRuntime();// on recupere une Runtime
        maCommande = maCommande + " -F " + pCOM; //ajout du peripherique
        maCommande = maCommande + " " + pBaud; //ajout de la vitesse de la liason
        maCommande = maCommande + " " + "cs" + pNbBits; //ajout du nombre de bits de la communication
        maCommande = maCommande + " -echo"; //on empeche l'echo local ce qui evite les doubles caractere
        if (pParite.equals("paire")) {
            maCommande = maCommande + " parenb -parodd"; //ajout de la parite paire et enlever la parite impaire
        }
        if (pParite.equals("impaire")) {
            maCommande = maCommande + " -parenb parodd";//ajout de la partie impaire et enlever la parite paire
        } else {
            pParite = maCommande + " -parenb -parodd";
        }
        try {
            Process pr = run.exec(maCommande);
        } catch (IOException e) {
            System.err.println("Impossible d'executer la commande: " + maCommande);
            System.err.println("la commande n'existe peut-etre pas");
            System.err.println("vous n'etes peut-etre pas root");
            e.printStackTrace(System.out);
            System.exit(1);
        }
    }
}
