package org.aconit.driver.base;

import org.aconit.util.myMessage;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * permet d'ecrire sur le port serie
 * @author Georges Schwing
 */
public class ecriture {

    //definition des variables utiles a l'execution du programme d'ecriture sur la liason serie
    private final int id; //variables contenant l'id du peripherique
    private final String com; //variables contenant e peripherique special de la liason serie
    private BufferedWriter bw; //buffer d'ecriture qui sera utiliser pour pouvoir ecrire sur la liason serie
    private myMessage ecrire; //message a ecrire sur la liason serie

    /**
     *
     * @param pId ID du peripherique
     * @param pCom indicatif de la prise serie sur /dev/ttyS?
     */
    public ecriture(int pId, String pCom) {
        id = pId;
        com = pCom;
    }

    /**
     * definition de la methode pour pouvoir ecrire sur la liason serie
     * @param pString message qui doit etre ecrit sur le peripherique
     */
    public void setMessage(String pString) {
        try {
            bw = new BufferedWriter(new FileWriter(com));
            bw.write(pString); //on marque dans le tampon le message qui devras etre envoyer dans le fichier
            bw.flush(); //on ecrit dans le fichier
            bw.close();
        } catch (IOException e) {
            System.err.println("impossible d'acceder au fichier: " + com);
            System.err.println("Verifier que ce fichier est accessible en ecriture");
            System.err.println("Verifier que le fichier existe");
            System.err.println("vous n'avec probablement pas les droits");
            e.printStackTrace(System.out);
            System.exit(1);
        }
    }
}
