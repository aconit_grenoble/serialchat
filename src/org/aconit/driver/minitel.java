package org.aconit.driver;

import org.aconit.driver.base.driverGeneric;

/**
 * permet d'ajouter un peripherique de type minitel
 * parametre de la connection:
 * <ul><li>1200 bauds</li>
 * <li>7 bits</li>
 * <li>parite paire</li>
 * <li>type ecran</li></ul>
 * @author Georges Schwing
 */
public final class minitel extends driverGeneric {

    /**
     * @param pId contient l'id qui seras attribuer au peripherique
     * @param pCom contient le port serie pour le peripherique /dev/ttyS
     */
    public minitel(int pId, String pCom) {
        super(pId, pCom, 7, 1200, "paire", "ecran");
        // aEcrire.setMessage(new String ( new byte[] {1B,3A,69,43}));
        cls(); //permet d'efface l'ecran, pour eviter d'avoir avant le redemarage du programme
        ASCIIart();
    }

    @Override
    public void CR() {
        aEcrire.setMessage(new String(new byte[]{0xD}));
    }

    @Override
    public void cls() {
        aEcrire.setMessage(new String(new byte[]{0x0C}));
    }

    @Override
    public void init() {
        aEcrire.setMessage(new String(new byte[]{0x1B, 0x3A, 0x69, 0x43}));
    }

    @Override
    public void effLigne() {
        CR();
        aEcrire.setMessage("                                        ");
    }
}
