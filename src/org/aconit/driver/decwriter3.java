package org.aconit.driver;

import org.aconit.driver.base.driverGeneric;

/**
 * Permet d'ajouter un peripherique de type Decwriter 3.
 * <br>
 * parapmetre de la connexion
 * <ul><li>9600 Bauds</li>
 * <li>7 bits</li>>
 * <li>type imprimante</li></ul>
 *
 * @author Georges Schwing
 */
public class decwriter3 extends driverGeneric {

    /**
     * @param pId contient l'ID du peripherique.
     * @param pCom contient la prise serie sur le modele /dev/ttyS?.
     */
    public decwriter3(int pId, String pCom) {
        super(pId, pCom, 7, 9600, "rien", "imprimante");
    }

    @Override
    public void CR() {
        aEcrire.setMessage(new String(new byte[]{0xD}));
    }
}
