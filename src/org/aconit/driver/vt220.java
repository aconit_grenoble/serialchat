package org.aconit.driver;

import org.aconit.driver.base.driverGeneric;

/**
 * permet d'ajouter un nouveau peripherique de type VT-220.
 * <br>
 * parametre de la connexion:
 * <ul><li>9600 Bauds</li>
 * <li>8 bits</li>
 * <li>parite paire</li>
 * <li>type ecran</li></ul>
 *
 * @author Georges Schwing
 */
public final class vt220 extends driverGeneric {

    /**
     *
     * @param pId contient l'ID du peripherique
     * @param pCom contient la prise serie sur le modele /dev/ttyS?
     */
    public vt220(int pId, String pCom) {
        super(pId, pCom, 8, 9600, "rien", "ecran");
        cls();
        ASCIIart();
    }

    @Override
    public void CR() {
        aEcrire.setMessage(new String(new byte[]{0xD}));
    }

    @Override
    public void cls() {
        for (int i = 0; i < 24; i++) {
            aEcrire.setMessage("\n");
            CR();
        }
    }
}
