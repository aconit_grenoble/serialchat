package org.aconit.driver;

import org.aconit.driver.base.driverGeneric;

/**
 * permet d'ajouter un nouveau peripherique de type wyse parametre de conexion
 * <ul><li>9600 Bauds</li>
 * <li>7 bits</li>
 * <li>parite aucune</li>
 * <li>type ecran<li></ul>
 *
 * @author Georges Schwing
 */
public final class wyse extends driverGeneric {

    /**
     * @param pId contient l'ID du peripherique
     * @param pCom contien la prise serie sur modele /dev/ttyS?
     */
    public wyse(int pId, String pCom) {
        super(pId, pCom, 7, 9600, "rien", "ecran");
        cls(); //permet de faire un effacement d'ecran au redemarage du programme
        ASCIIart();
    }

    @Override
    public void CR() {
        aEcrire.setMessage(new String(new byte[]{0xD})); //code permettant de faire un retour a la ligne CR
    }

    @Override
    public void cls() {
        for (int i = 0; i < 24; i++) {
            aEcrire.setMessage("\n");
            CR();
        }
    }
}
