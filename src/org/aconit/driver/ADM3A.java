package org.aconit.driver;

import org.aconit.driver.base.driverGeneric;

/**
 * permet d'ajouter un nouveau peripherique de type ADM-3A parametre de
 * connexion:
 * <ul><li>9600 Bauds</li>
 * <li>7 bits</li>
 * <li>parite paire</li>
 * <li>type ecran<//li></ul>
 *
 * @author Georges Schwing
 */
public final class ADM3A extends driverGeneric {

    /**
     * @param pId conntient l'ID du peripherique
     * @param pCom contient la prise serie sur le modele /dev/ttyS?
     */
    public ADM3A(int pId, String pCom){
        super(pId,pCom,7,9600,"paire","ecran");
        cls();
        ASCIIart();
    }
    
    @Override
    public void CR() {
        aEcrire.setMessage(new String(new byte[]{0xD}));
    }
    
    @Override
    public void cls(){
        aEcrire.setMessage(new String(new byte[]{0x1E, 0x1B,0x79}));
    }
}
