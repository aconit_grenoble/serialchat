package org.aconit.util;

/**
 * Cette class permet de tetecter le type d'OS sur lequel est execute le programme.
 * @author Georges Schwing
 */
public class osType {

    private String OS; //contient le type d'os de la machine qui execute le system.

    /**
     * Constructeur par default.
     */
    public osType() {
        this.OS = System.getProperty("os.name").toLowerCase();
    }

    /**
     * Indique si il s'agit d'un OS de type Microsoft.
     * @return true si l'Os est de type Microsoft.
     */
    public boolean isWindows() {
        if (OS.indexOf("win") >= 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Indique si il s'agit d'un OS de type Unix/Linux.
     * @return true si il s'agit d'un OS linux/Unix.
     */
    public boolean isUnix() {
        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") >= 0);
    }

    /**
     * Indique si il s'agit d'un Os de type Solaris ou sunOs.
     * @return true si il s'agit d'un OS Solaris/SunOs.
     */
    public boolean isSolaris() {
        return (OS.indexOf("sunos") >= 0);
    }

    /**
     * Indique si il s'agit d'un OS de type MAC.
     * @return true si il s'agit d'un OS de type MAC.
     */
    public boolean isMac() {
        return (OS.indexOf("mac") >= 0);
    }
}
