package org.aconit.util;

import java.io.Reader;
import java.io.IOException;
import org.aconit.driver.base.lectureThread;

/**
 *Cette class est une reecriture de BufferedReader
 * @see BufferedReader
 * @author Georges Schwing
 */
public class myBfReader extends Reader {

    private Reader in;
    private char cb[];
    private int nChars, nextChar;
    private static final int INVALIDATED = -2;
    private static final int UNMARKED = -1;
    private int markedChar = UNMARKED;
    private int readAheadLimit = 0; /* Valid only when markedChar > 0 */

    private boolean skipLF = false; //si le caractere est un linefeed on le saute
    private boolean markedSkipLF = false; //marque pour le 
    private static int defaultCharBufferSize = 8192;
    private static int defaultExpectedLineLength = 80;
    private lectureThread lctThd;

    /**
     * Permet de cree un buffer tout en precisant la taille de ce buffer
     * @param in un Reader
     * @param sz la taille du buffer
     */
    public myBfReader(Reader in, int sz) {
        super(in);
        if (sz <= 0) {
            throw new IllegalArgumentException("Buffer size <= 0");
        }
        this.in = in;
        cb = new char[sz];
        nextChar = nChars = 0;
    }

    /**
     * Creer un buffer
     * @param in un Reader
     */
    public myBfReader(Reader in) {
        this(in, defaultCharBufferSize);
    }

    public myBfReader(Reader in, lectureThread plec) {
        this(in, defaultCharBufferSize);
        this.lctThd = plec;
    }

    /**
     * verifie si le buffer est ouvert ou fermer
     * @throws IOException
     */
    private void ensureOpen() throws IOException {
        if (in == null) {
            throw new IOException("Stream closed");
        }
    }

    /**
     * Permet de remplir le buffer met la marque si c'est valide
     * @throws IOException
     */
    private void fill() throws IOException {
        int dst;
        if (markedChar <= UNMARKED) {
            /* No mark */
            dst = 0;
        } else {
            /* Marked */
            int delta = nextChar - markedChar;
            if (delta >= readAheadLimit) {
                /* Gone past read-ahead limit: Invalidate mark */
                markedChar = INVALIDATED;
                readAheadLimit = 0;
                dst = 0;
            } else {
                if (readAheadLimit <= cb.length) {
                    /* Shuffle in the current buffer */
                    System.arraycopy(cb, markedChar, cb, 0, delta);
                    markedChar = 0;
                    dst = delta;
                } else {
                    /* Reallocate buffer to accommodate read-ahead limit */
                    char ncb[] = new char[readAheadLimit];
                    System.arraycopy(cb, markedChar, ncb, 0, delta);
                    cb = ncb;
                    markedChar = 0;
                    dst = delta;
                }
                nextChar = nChars = delta;
            }
        }
        int n;
        do {
            n = in.read(cb, dst, cb.length - dst);
        } while (n == 0);
        if (n > 0) {
            nChars = dst + n;
            nextChar = dst;
        }
    }

    /**
     * permet de lire un caractere
     * @return renvoi le caractere lue sous la forme d'un entier compris entre 0 et 65535 ou -1 si la fin de la chaine de caractere est atteinte
     * @throws IOException
     */
    @Override
    public int read() throws IOException {
        synchronized (lock) {
            ensureOpen();
            for (;;) {
                if (nextChar >= nChars) {
                    fill();
                    if (nextChar >= nChars) {
                        return -1;
                    }
                }
                if (skipLF) {
                    skipLF = false;
                    if (cb[nextChar] == '\n') {
                        nextChar++;
                        continue;
                    }
                }
                return cb[nextChar++];
            }
        }
    }

    /**
     * Lit des caractères dans une partie d'un tableau, depuis un flux sous-jacent si nécessaire.
     * @param cbuf tableau de caractere
     * @param off
     * @param len
     * @return
     * @throws IOException
     */
    private int read1(char[] cbuf, int off, int len) throws IOException {
        if (nextChar >= nChars) {
            /* If the requested length is at least as large as the buffer, and
            if there is no mark/reset activity, and if line feeds are not
            being skipped, do not bother to copy the characters into the
            local buffer.  In this way buffered streams will cascade
            harmlessly. */
            if (len >= cb.length && markedChar <= UNMARKED && !skipLF) {
                return in.read(cbuf, off, len);
            }
            fill();
        }
        if (nextChar >= nChars) {
            return -1;
        }
        if (skipLF) {
            skipLF = false;
            if (cb[nextChar] == '\n') {
                nextChar++;
                if (nextChar >= nChars) {
                    fill();
                }
                if (nextChar >= nChars) {
                    return -1;
                }
            }
        }
        int n = Math.min(len, nChars - nextChar);
        System.arraycopy(cb, nextChar, cbuf, off, n);
        nextChar += n;
        return n;
    }

    /**
     * Lit des caractères dans une partie d'un tableau.
     * <br>
     * <br>
     * Cette méthode implémente le contrat général de la méthode de lecture correspondant de la classe Reader.
     * Comme un confort supplémentaire , il tente de lire autant de caractères que possible en appelant à
     * plusieurs reprises la méthode de lecture de flux sous-jacent . Cette réitéré lu continue
     * jusqu'à ce que l'une des conditions suivantes soit remplie:
     * <ul>
     *<li>Le nombre spécifié de caractères ont été lus ,</li>
     *<li>La méthode de lecture de flux sous-jacent renvoie -1 , indiquant la fin de fichier , ou</li>
     *<li>La méthode prêt de flux sous-jacent retourne false , indiquant que de nouvelles demandes d'entrée se bloquer .</li>
     * </ul>
     *<br>
     *Si la première lecture sur les retours de flux sous-jacents -1 pour indiquer en fin de fichier, cette méthode renvoie -1 .
     * Sinon, cette méthode renvoie le nombre de caractères réellement lus.
     *<br>
     *Les sous-classes de cette classe sont encouragés , mais pas nécessaire , pour tenter de lire autant de caractères que possible de la même façon .
     *Normalement cette méthode prend caractères de la mémoire tampon de caractères de ce courant , le remplir de flux sous-jacent si nécessaire.
     * Si, par contre , le tampon est vide , la marque n'est pas valide, et la longueur demandée est au moins aussi grand que la mémoire tampon ,
     * cette méthode va lire les caractères directement à partir du flux sous-jacent dans la rangée donnée. BufferedReaders
     * Ainsi redondants ne seront pas copier des données inutilemen
     * @param cbuf tampon de destination
     * @param offset à partir duquel commencer stocker des caractères
     * @param len nombre maximum de caractères à lire
     * @return  Le nombre de caractères lus , ou -1 si la fin du flux a été atteinte
     * @throws IOException IOException Si une erreur d'entre sortie se produit
     */
    public int read(char cbuf[], int off, int len) throws IOException {
        synchronized (lock) {
            ensureOpen();
            if ((off < 0) || (off > cbuf.length) || (len < 0)
                    || ((off + len) > cbuf.length) || ((off + len) < 0)) {
                throw new IndexOutOfBoundsException();
            } else if (len == 0) {
                return 0;
            }
            int n = read1(cbuf, off, len);
            if (n <= 0) {
                return n;
            }
            while ((n < len) && in.ready()) {
                int n1 = read1(cbuf, off + n, len - n);
                if (n1 <= 0) {
                    break;
                }
                n += n1;
            }
            return n;
        }
    }

    /**
     *Lit une ligne de texte. Une ligne est considérée comme terminée par un quelconque d'un saut de ligne ('\ n'),
     * un retour chariot ('\ r'), ou un retour chariot immédiatement suivi par un saut de ligne.
     * <br>
     * @param ignoreLF Si ignoreLF est vrai, le prochain '\ n' (retour a la ligne) est sautée
     * @return Une chaîne contenant le contenu de la ligne,
     * sauf les caractères de terminaison de ligne, ou null si la fin du flux a été atteint
     * @throws IOException  Si une erreur d'entre/sortie se produit
     * @see LineNumberReader.readLine ()
     */
    String readLine(boolean ignoreLF) throws IOException {
        StringBuffer s = null;
        int startChar;
        synchronized (lock) {
            ensureOpen();
            boolean omitLF = ignoreLF || skipLF;
            bufferLoop:
            for (;;) {
                if (nextChar >= nChars) {
                    //on recharge le buffer. C'est a dire qu'il y a des nouveau caractere en memoire.
                    lctThd.newChar();
                    fill();
                }
                if (nextChar >= nChars) { /* EOF */
                    if (s != null && s.length() > 0) {
                        return s.toString();
                    } else {
                        return null;
                    }
                }
                boolean eol = false;
                char c = 0;
                int i;
                /* Skip a leftover '\n', if necessary */
                if (omitLF && (cb[nextChar] == '\n')) {
                    nextChar++;
                }
                skipLF = false;
                omitLF = false;
                charLoop:
                for (i = nextChar; i < nChars; i++) {
                    c = cb[i];
                    if ((c == '\n') || (c == '\r')) {
                        eol = true;
                        break charLoop;
                    }
                }
                startChar = nextChar;
                nextChar = i;
                if (eol) {
                    String str;
                    if (s == null) {
                        str = new String(cb, startChar, i - startChar);
                    } else {
                        s.append(cb, startChar, i - startChar);
                        str = s.toString();
                    }
                    nextChar++;
                    if (c == '\r') {
                        skipLF = true;
                    }
                    return str;
                }
                if (s == null) {
                    s = new StringBuffer(defaultExpectedLineLength);
                }
                s.append(cb, startChar, i - startChar);
            }
        }
    }

    /**
     * Lit une ligne de texte. Une ligne est considérée comme terminée par un quelconque d'un saut de ligne ('\ n'),
     * un retour chariot ('\ r'), ou un retour chariot immédiatement suivi par un saut de ligne.
     * @return Une chaîne contenant le contenu de la ligne, sauf les caractères de terminaison de ligne, ou null si la fin du flux a été atteint
     * @throws IOException IOException  Si une erreur d'entre/sortie se produit
     */
    public String readLine() throws IOException {
        return readLine(false);
    }

    /**
     * Saute des caractères.
     * @param n Le nombre de caractères à sauter
     * @return Le nombre de caractères réellement sautée
     * @throws IOException Si une erreur d'entre/sortie se produit
     */
    @Override
    public long skip(long n) throws IOException {
        if (n < 0L) {
            throw new IllegalArgumentException("skip value is negative");
        }
        synchronized (lock) {
            ensureOpen();
            long r = n;
            while (r > 0) {
                if (nextChar >= nChars) {
                    fill();
                }
                if (nextChar >= nChars) /* EOF */ {
                    break;
                }
                if (skipLF) {
                    skipLF = false;
                    if (cb[nextChar] == '\n') {
                        nextChar++;
                    }
                }
                long d = nChars - nextChar;
                if (r <= d) {
                    nextChar += r;
                    r = 0;
                    break;
                } else {
                    r -= d;
                    nextChar = nChars;
                }
            }
            return n - r;
        }
    }

    /**
     * Indique si ce flux est prêt à être lu.
     * Un flux de caractères en mémoire tampon est prêt si le tampon n'est pas vide, ou si le flux de caractères sous-jacent est prêt.
     * @return renvoi truu si le tampon est pret, false si non
     * @throws IOException Si une erreur d'entre/sortie se produit
     */
    @Override
    public boolean ready() throws IOException {
        synchronized (lock) {
            ensureOpen();
            /*
             * If newline needs to be skipped and the next char to be read
             * is a newline character, then just skip it right away.
             */
            if (skipLF) {
                /* Note that in.ready() will return true if and only if the next
                 * read on the stream will not block.
                 */
                if (nextChar >= nChars && in.ready()) {
                    fill();
                }
                if (nextChar < nChars) {
                    if (cb[nextChar] == '\n') {
                        nextChar++;
                    }
                    skipLF = false;
                }
            }
            return (nextChar < nChars) || in.ready();
        }
    }

    /**
     * Indique si ce flux prend en charge la marque mark()
     * @return true si c'est supporter, false sinon
     */
    public boolean markSupported() {
        return true;
    }

    /**
     * Marque la position actuelle dans le flux. Les appels suivants pour réinitialiser () va tenter de repositionner le courant à ce point.
     * @param readAheadLimit Limite portant sur ​​le nombre de caractères qui peuvent être lues tout en conservant la marque.
     * Une tentative pour rétablir le courant après la lecture des caractères jusqu'à cette limite ou au-delà peut échouer.
     * Une valeur de limite supérieure à la taille de la mémoire tampon d'entrée entraînera un nouveau tampon à allouer dont la taille
     * n'est pas inférieure à la limite. Par conséquent les grandes valeurs doivent être utilisés avec précaution.
     * @throws IOException Si une erreur d'entre/sortie se produit
     */
    @Override
    public void mark(int readAheadLimit) throws IOException {
        if (readAheadLimit < 0) {
            throw new IllegalArgumentException("Read-ahead limit < 0");
        }
        synchronized (lock) {
            ensureOpen();
            this.readAheadLimit = readAheadLimit;
            markedChar = nextChar;
            markedSkipLF = skipLF;
        }
    }

    /**
     * Réinitialise le flux de la marque la plus récente.
     * @throws IOException si le flux n'a jamias etait marquer ou si la marque est invalide
     */
    @Override
    public void reset() throws IOException {
        synchronized (lock) {
            ensureOpen();
            if (markedChar < 0) {
                throw new IOException((markedChar == INVALIDATED)
                        ? "Mark invalid"
                        : "Stream not marked");
            }
            nextChar = markedChar;
            skipLF = markedSkipLF;
        }
    }

    /**
     * ferme le flux actuel
     * @throws IOException Si une erreur d'entre/sortie se produit
     */
    public void close() throws IOException {
        synchronized (lock) {
            if (in == null) {
                return;
            }
            in.close();
            in = null;
            cb = null;
        }
    }
}
