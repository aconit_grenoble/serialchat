package org.aconit.util;

/** Objet servant à transporter toutes les informations utiles à la transmission d'un message entre les programmes d'entrée sortie et le programme principal.
 * Il ne faudra pas oublier d'effectuer un transtypage de type
 * 								(myMessage) pArgs
 * dans la méthode update pour pouvoir récupérer le message transmit, car ce message est automatiquement typé comme un objet de type Object
 *
 * 		Liste des Codes
 * <table>
 * <tr>
 * <th>code</th>
 * <th>signification</th>
 * </tr>
 * <tr><td>0000</td>                            <td>Envoi le message du programme d'entre-sortie vers le programme central</td></tr>
 * <tr><td>0001</td>                            <td>Envoi le message du programme central vers le programme d'entre-sortie</td></tr>
 * <tr><td>0002</td>                            <td>Envoi la liste des Id[]</td></tr>
 * <tr><td>0003</td>                            <td>Message d'erreur du à un mauvais ID envoyer depuis le programme principal vers les périphériques</td></tr>
 * <tr><td>0004</td>                            <td>Message envoyer a soit même</td></tr>
 * <tr><td>0005</td>                            <td>Permet d'obtenir une nouvelle ID</td></tr>
 * <tr><td>0006</td>                            <td>Permet de libérer l'ID de l'émetteur</td></tr>
 * <tr><td>0007</td>                            <td>Permet de changer le nom de la machine</td></tr>
 * </table>
 */
public class myMessage {

    //definition des differentes variables qui seront utile au transport de l'information
    final private int destId; //cette variable vas contenir l'id du programme emeteur du message
    final private int code; //cette variables vas contenir les differents code d'instruction a executer dans le programme principale
    final private String message; //cette variable vas contenir le message cet a dire l'information qui est transmit avec le code de la variable plus haut
    final private int emeId; //cette variable contient l'id de l'emetteur
    private String[] lstId; //cette variable contient la liste des id
    private boolean bool; //permet d'indiquer un booleen en plus

    /**
     * permet de creer un nouveau objet de type myMessage
     * @param pEmeId ID de l'émetteur du message
     * @param pDestId ID du déstinataire du message
     * @param pCode code associer au message
     * @param pMessage message qui est transmit
     */
    public myMessage(int pEmeId, int pDestId, int pCode, String pMessage) {
        this.emeId = pEmeId;
        this.destId = pDestId;
        this.code = pCode;
        this.message = pMessage;
    }

    /**
     * Permet d'indiquer un booléen en plus dans le message. Ce qui est utile pour definir le type d'ID qui est demander.
     * @param pBool booléen que l'on veut transmettre.
     */
    public void setBool(boolean pBool) {
        bool = pBool;
    }

    /**
     * Permet de récupérer le booléen qui est éventuellement transmit dans le message.
     * @return le booléen que l'on veut recuperer.
     */
    public boolean getBool() {
        return (bool);
    }

    /**
     * Méthode permettant d'ecrire la liste des IDs.
     * @param pLstId contient la liste des IDs.
     */
    public void setLstId(String[] pLstId) {
        this.lstId = pLstId;
    }

    /**
     *Méthode permettant de renvoyer la liste des IDs.
     * @return renvoi la liste des IDs.
     */
    public String[] getLstId() {
        return (this.lstId);
    }

    /**
     * Méthode retournant l'ID du destinataire.
     * @return renvoi l'ID du destinataire du message.
     */
    public int getDestId() {
        return (this.destId);
    }

    /**
     * Méthode permettant de recuperer l'ID de l'émetteur.
     * @return renvoi lID de l'emetteur.
     */
    public int getEmeId() {
        return (this.emeId);
    }

    /**
     * Méthode retournant le code du message.
     * @return renvoi le code du message.
     */
    public int getCode() {
        return (this.code);
    }

    /**
     * Méthode retournant le message en lui même.
     * @return renvoi le message en lui même.
     */
    public String getMessage() {
        return (this.message);
    }

    /**
     * Méthode permettant d'obtenir un message pre-formaté pour pouvoir directement l'afficher sur un des terminaux.
     * @return renvoi le message ainsi formaté.
     * @param pMess permet de definir si il s'agit d'un message verbeux ou non
     */
    public String getString(boolean pMess) {
        if (pMess == true) {
            String texte = this.lstId[this.emeId] + " vers " + this.lstId[this.destId] + ": " + this.message + "\n";
            return (texte);
        } else {
            if (this.emeId < 10) {
                String texte = this.emeId + "  -> " + this.destId + " :" + this.message + "\n";
                return (texte);
            } else {
                String texte = this.emeId + " -> " + this.destId + " :" + this.message + "\n";
                return (texte);
            }
        }
    }
}

