package org.aconit.util;

import org.aconit.driver.base.driverGeneric;

/**
 * permet de faire un compteur de une minute qui est utiliser pour l'autorisation d'ecrire sur le peripherique
 * @author Georges Schwing
 */
public class duree implements Runnable {

    private int itr; //contient le nombre d'iteration
    private driverGeneric driver; //contient le driver a prevenir
    private int tps = 200;//temps a attendre 600 mili, 1 minute

    /**
     * permet d'initialiser le compteur
     * @param pDriver contient le driver qui a instancier ce compteur
     */
    public duree(driverGeneric pDriver) {
        driver = pDriver;
        itr = 0;
    }

    /**
     * permet de remaitre a zero le compteur
     */
    public void reset() {
        itr = 0;
    }

    /**
     * permet de rendre possible l'utilisation de l'ecriture sur le terminal
     */
    public void allow() {
        itr = tps + 42;
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
            if (itr >= tps) {
                driver.setAuto(true);
            } else {
                driver.setAuto(false);
            }
            itr++;

        }
    }
}
