
import org.aconit.util.myMessage;
import org.aconit.util.osType;
import org.aconit.gui.interfaceGUI;
import org.aconit.gui.ChatClientGUI;
import org.aconit.driver.*;
import org.aconit.driver.base.*;
import org.aconit.net.server.netServer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Comment;
import org.xml.sax.SAXException;
import java.util.Observer;
import java.util.Observable;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.text.SimpleDateFormat;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Button;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys;

/**
 * structure code renvoyer par les sous programmes
 *
 *
 * Liste des Codes
 * <table>
 * <tr>
 * <th>code</th>
 * <th>signification</th>
 * </tr>
 * <tr><td>0000</td> <td>Envoi message du programme d'entre-sortie vers le
 * programme central</td></tr>
 * <tr><td>0001</td> <td>Envoi message du programe central vers le programme
 * d'entre-sortie</td></tr>
 * <tr><td>0002</td> <td>Envoi la liste des Id[]</td></tr>
 * <tr><td>0003</td> <td>Message d'erreur dut a un mauvais ID envoyer depuis le
 * programme principal vers les peripheriques</td></tr>
 * <tr><td>0004</td> <td>Message envoyer a soit meme</td></tr>
 * <tr><td>0005</td> <td>Permet d'obtenir un nouvelle ID</td></tr>
 * <tr><td>0006</td> <td>Permet de liberer l'id de l'emetteur</td></tr>
 * <tr><td>0007</td> <td>Permet de changer le nom de la machine</td></tr>
 * </table>
 *
 * @author Georges Schwing
 */
public final class serialChat implements Observer {

    //definition de toutes les variables necessaires aux fonctionnement du programme central
    private final String[] id = new String[100]; //represente le nom qui correspont aux diffentes programmes d'entree sortie
    private final String[] idCom = new String[100]; //vas indiquer le nom des differentes prises COM du system
    private interfaceGUI gui; // definit la couche graphique
    private boolean allowGui; //permet de dire si on veut ou pas l'interface graphique
    private boolean allowEcho; //permet d'autoriser l'echo sur la console
    private boolean isServer; //indique si il s'agit d'un serveuril s'agit du choix par default
    private boolean isClient; //indique si il s'agit d'un client
    private final driverGeneric[] periph = new driverGeneric[99];
    //liste des bouts de la fenetre pour choisir de mode
    private Frame fenetre; //il s'agit de la fenetre en elle meme
    private Button serverBtn; //il s'agit du bouton server de la fenetre choix de type
    private Button clientBtn; //il s'agit du bouton client de la fenetre choix de type
    //liste des variables utiliser pour la communication sur le reseaux
    private netServer netServ; //contient le serveur reseau
    private ChatClientGUI netClt; //contient le client reseau
    //contient la liste des fficchier et dossier utilise par le programme
    private final File repLog; //contient le dossier aui contient le dossier avec les logs des messages = new File(System.getProperty("user.home") + "/.serialChat/configuration");
    private final File messLog; //contient les logs des messages
    private File messConf; //contient le fichier de conf

    /**
     * Definition du main necessaire au lancement de l'application
     *
     * @param pArgs permet de faire passer les parametres d'executions aux
     * programmes
     */
    public static void main(String[] pArgs) {
        serialChat serialChat = new serialChat(pArgs);
    }

    /**
     * Constructeur par default qui permet d'executer le programme
     *
     * @param pArgs contient les parametres d'executions qui ont etaient passes
     * au programme
     */
    public serialChat(String[] pArgs) {
        super();
        repLog = new File(System.getProperty("user.home") + "/.serialChat/logs");
        messLog = new File(repLog.getPath() + "/messages_logs.log");
        ClassLoader cl = this.getClass().getClassLoader();
        id[0] = "broadcast";
        id[99] = "ordinateur";
        allowEcho = true;
        allowGui = true;
        if (pArgs.length > 0) {//que faire si il y a un parametre ou plus qui est passer au programme
            boolean isMode = false; //permet de savoir si on a choisit deux modes differents
            boolean isWrong = true; //permet de dire si il y a un parametre qui ne convient pas
            for (int i = 0; i <= pArgs.length - 1; i++) {
                if (pArgs[i].equals("noEcho") || pArgs[i].equals("noGui") || pArgs[i].equals("server") || pArgs[i].equals("client")) {
                    if (pArgs[i].equals("noEcho")) { //on verifie que l'echo sur la console n'est pas desactiver
                        allowEcho = false;
                    }
                    if (pArgs[i].equals("noGui")) { //on verifie que le parametre passer est egal a noGui
                        //dans ce cas on n'affiche pas l'interface graphique
                        allowGui = false;
                    }
                    if (pArgs[i].equals("server")) { //que faire lorsque l'on demande une instance de type server
                        isServer = true;
                        if (isMode) {
                            System.err.println("Les options client et server sont incompatible");
                            System.exit(1);
                        }
                        isMode = true;
                    }
                    if (pArgs[i].equals("client")) { //que faire lorsque l'on demande une instance de type client
                        isClient = true;
                        if (isMode) {
                            System.err.println("Les options client et server sont incompatible");
                            System.exit(1);
                        }
                        isMode = true;
                    }
                } else { // le paramettre n'est pas un parametre qui existe.
                    //om affiche donc les parametres possible et on quitte
                    System.err.println(pArgs[i] + " n'est pas un parametre valide");
                    isWrong = false;
                }
                if (isClient && !allowGui) { //que faire si on demande un type client sans interface graphique
                    System.err.println("Les options client et noGui sont incompatibles");
                    isWrong = false;
                }
            }
            //maintenant que l'on a fait le tour de tout les parametres
            //on verfie qu'il n'y avait pas de parametre(s) eronne(s)
            if (!isWrong) {
                System.err.println();
                System.err.println("java -jar serialChat.jar [noGui] [noEcho] [server|client]");
                System.exit(1);
            }
        }
        if (isServer == isClient) { //que faire si on demade une instance de type client et de type server
            if (allowGui) {
                //on lance la fenetre de choix de mode
                fenetre = new Frame("choisir le Mode");
                fenetre.setSize(300, 150);
                fenetre.setIconImage(Toolkit.getDefaultToolkit().getImage(cl.getResource("aconit.png")));
                fenetre.addWindowListener(new WindowAdapter() {

                    @Override
                    public void windowClosing(WindowEvent pEv) {
                        System.exit(0);
                    }
                });
                fenetre.setLayout(new GridLayout(1, 2));
                serverBtn = new Button("Serveur");
                serverBtn.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent pEv) {
                        isServer = true;
                        init();
                        fenetre.setVisible(false);
                    }
                });
                fenetre.add(serverBtn);
                clientBtn = new Button("Client");
                clientBtn.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent pEv) {
                        isClient = true;
                        init();
                        fenetre.setVisible(false);
                    }
                });
                fenetre.add(clientBtn);
                fenetre.setVisible(true);
            } else {
                System.err.println("si vous specifier l'option noGui vous devez indiquer si il s'agit d'un type server ou client");
                System.exit(1);
            }
        }
        init();
    }

    /**
     * Prmet de lancer le processus de lancement a proprement parlert une fois
     * que le choix du type de serialChat est fait.
     */
    public void init() {
        initXML();
        if (isClient) { //que faire lorsque le serialChat est executer en tant que client
            //on lance le client sur le reseau
            netClt = new ChatClientGUI();
        }
    }

    /**
     * definition de la classe abstraite servant a l'ecoute des objet envoyer
     * par les objet observer
     *
     * @param p0 non utiliser ici
     * @param Pargs contient l'objet qui est transmit, cet object est de type
     * myMessage
     */
    public void update(Observable p0, Object Pargs) {
        myMessage recus = (myMessage) Pargs;
        recus.setLstId(this.id);
        myMessage message; //message qui vas etre envoyer
        //utilisation d'une instruction a choix multiple switch pour determnier quoi faire en fonction du code recus

        switch (recus.getCode()) {
            case 0000: //message recus d'un peripherique d'entree sortie vers le programme central
                myMessage goodMessage = new myMessage(recus.getEmeId(), recus.getDestId(), 0001, recus.getMessage());
                goodMessage.setLstId(this.id);
                myMessage badMessage = new myMessage(recus.getEmeId(), recus.getDestId(), 0003, recus.getMessage());
                badMessage.setLstId(this.id);
                //il faut renvoyer vers l'emetteur le message si ce n'est pas l'interface graphique qui envoi

                if (recus.getEmeId() != 99 && id[recus.getDestId()] != null && recus.getEmeId() != recus.getDestId()) {
                    periph[recus.getEmeId()].append(recus);// de plus on le renvoi vers l'emmeteur

                }
                if (id[recus.getDestId()] == null) { //que faire si l'ID de destination est invalide
                    //on envoi au system qui l'a envoyer un message d'erreur pour lui dire que son ID de destination est invalide
                    ecriture(badMessage.getString(true), true);
                    if (recus.getEmeId() == 99) {
                        gui.append(badMessage);

                    } else {
                        periph[recus.getEmeId()].append(badMessage);
                        netServ.append(badMessage);

                    }
                } else {
                    ecriture(goodMessage.getString(true), true);
                    //le message est correcte on le transmet a la persone qui en a besoin
                    if (recus.getEmeId() != recus.getDestId()) {
                        //on le transmet aussi a l'interface graphique car elle affiche l'historique des message qui ont etait transmit
                        //mais on peut uniq`uement le transmettre si l'interface graphique a ete instancier
                        if (allowGui) {
                            gui.append(goodMessage);

                        } //et on l'affiche aussi sur la console ce qui permet de logger les messages si on le desire
                        //de plus on met le parametre true ce qui permet d'avoir des logs verbeux qui sont donc plus lisible
                        //et on rajoute en debut de ligne l'id en decimal pour plus de lisibilite
                        if (allowEcho) {
                            System.out.println(recus.getEmeId() + "     " + goodMessage.getString(true));

                        }
                    }
                    if (recus.getDestId() != 99 && recus.getEmeId() != recus.getDestId() && recus.getDestId() != 00) {
                        periph[recus.getDestId()].append(goodMessage);
                        netServ.append(goodMessage);

                    }
                    if (recus.getEmeId() == recus.getDestId()) {
                        if (recus.getDestId() < 99) {
                            periph[recus.getDestId()].append(new myMessage(99, recus.getEmeId(), 0004, "Impossible de s'envoyer un message a soit meme"));
                            netServ.append(new myMessage(99, recus.getEmeId(), 0004, "Impossible de s'envoyer un message a soit meme"));

                        } else {
                            if (allowGui) {
                                gui.append(new myMessage(99, recus.getEmeId(), 0004, "Impossible de s'envoyer un message a soit meme"));

                            }
                        }
                    }
                    if (recus.getDestId() == 00) { //que faire en cas de broadcast
                        netServ.append(goodMessage);

                        for (int i = 1; i
                                < 99; i++) {
                            if (i != recus.getEmeId() && id[i] != null) {
                                periph[i].append(goodMessage);

                            }
                        }
                    }
                }
                //envoi du message vers l'interface graphique
                break;

            case 0001: //envoi du programme principal vers les programmes d'entrer sorties
                //pas utile pour le moment car le traitement et le reenvois de l'information ce fait dans le cas au dessu
                break;

            case 0002: //envoi la liste des Id[]
                message = new myMessage(recus.getEmeId(), recus.getDestId(), 0002, null);
                message.setLstId(this.id);

                if (recus.getEmeId() == 99) {
                    gui.append(message);

                } else {
                    periph[recus.getEmeId()].append(message);

                }
                netServ.append(message);

                break;

            case 0005: //permet d'obtenir un nouvelle ID
                int newId = getNewId(recus.getMessage(), recus.getBool());
                message = new myMessage(recus.getEmeId(), recus.getDestId(), 0005, Integer.toString(newId));
                if (recus.getBool()) {
                    ecriture("nouveau peripherique RS-232 ajoute avec l'ID: " + message.getMessage(), true);
                } else {
                    ecriture("nouveau client reseau ajoute avec l'ID: " + message.getMessage(), true);
                }
                if (recus.getEmeId() == 100) { //que faire si la demande concerne le serveur reseau
                    netServ.append(message);
                }
                if (newId != 0) { //que faire si il y a un nouvelle ID
                    id[newId] = recus.getMessage();
                    periph[newId] = new driverGeneric();
                }
                break;

            case 0006: //que faire lorsque l'on veut liberer lID de l'emetteur
                releaseId(recus.getEmeId());
                ecriture("Le peripherique avec L'ID: " + recus.getEmeId() + "et le nom: " + id[recus.getEmeId()] + "viens de partir", true);
                break;

            case 0007: //que faire lorsque une demande de changement de nom est emise
                if (allowEcho) {
                    System.out.println("L'ID " + recus.getEmeId() + "qui s'appelait" + id[recus.getEmeId()] + "s'appelle maintenant: " + recus.getMessage());
                }
                id[recus.getEmeId()] = recus.getMessage();
                ecriture("Le peripherique avec L'ID: " + recus.getEmeId() + "s'appelle maintenant: " + id[recus.getEmeId()], true);
                break;
        }
    }

    /**
     * Methode permetant de definir un nouveau ID pour chaque nouveau
     * peripherique
     *
     * @param pArgs nom qui seras attribuer au nouvelle ID
     * @param type true pour un peripherique, false pour un ordinateur
     * @return ID disponible qui est renvoyer apres obtention
     */
    public int getNewId(String pArgs, boolean type) {
        boolean idPasDispo = true;

        int pId = 0;

        if (type) {
            //demande d'un id pour un peripherique RS232
            for (int i = 0; idPasDispo;
                    i++) { //boucle servant a la recherche d'un ID disponible
                if (i < id.length) { //si plus grand il n'y a pas d'ID disponible
                    if (id[i] == null) { //c'est a dire que l'Id est dispo
                        idPasDispo = false;
                        pId = i;
                        id[i] = pArgs; //on applique le nom du peripherique comme nom lie a l'ID

                        if (allowEcho) {
                            System.out.println("Le nouveau peripherique: " + pArgs + " dispose de l'ID: " + i);

                        }
                    }
                } else {
                    System.err.println("IL N'Y A PLUS D'ID DISPONIBLE, VOUS AVEZ PLUS DE 90 PERIPHERIQUES !!!!!!!");
                    idPasDispo = false; //on sort de la boucle meme si il n'y a plus d'ID dispo

                }
            }
        } else {
            //demande d'un ID pour un client reseaux
            for (int i = 98; idPasDispo;
                    i--) {
                if (i > 0) { //si in ferrieur il n'y a plus d'id
                    if (id[i] == null) { //c'est a dire que l'id est dispo
                        idPasDispo = false;
                        pId = i;
                        id[i] = pArgs; // on rentre le nom du client reseaux

                        if (allowEcho) {
                            System.out.println("Le nouveau client reseau: " + pArgs + " dispose de l'ID: " + i);

                        }
                    }
                } else {
                    System.err.println("IL N'Y A PLUS D'ID DISPONIBLE, VOUS AVEZ PLUS DE 90 PERIPHERIQUES !!!!!!!");
                    idPasDispo = false; //on sort de la boucle meme si il n'y a plus d'ID dispo

                }
            }
        }
        return (pId);

    }

    /**
     * methode permettant de rendre disponible un ID et donc de supprimer un
     * peripherique
     *
     * @param pId ID que l'on veut rendre disponible
     */
    public void releaseId(int pId) {
        if (allowEcho) {
            System.out.println("Liberation de l'ID: " + pId);

        }
        id[pId] = null;

    }

    /**
     * Permet de faire l'initialisation de tout le bazard qui est utilises pour
     * pouvoir s'occupper de la gestion des fichiers XML de configuration des
     * peripheriques connecte a la machine.
     */
    public void initXML() {
        //recherche du chemin d'acces aux repertoire personnelle de l'utilisateur
        System.out.println(System.getProperty("user.home"));
        //puis verification de la  presence ou non du repertoire caches de l'application avec creation le cas echeant
        File rep = new File(System.getProperty("user.home") + "/.serialChat");
        File repConf = new File(System.getProperty("user.home") + "/.serialChat/configuration");
        if (!rep.exists()) {
            //Le repertoire n'existe pas donc creation ddu repertoire
            rep.mkdir();
        }
        //creation ou non du repertoire ccontenant la configuration
        if (!repConf.exists()) {
            repConf.mkdir();
        }
        if (!repLog.exists()) {
            repLog.mkdir();
        }
        //creation du fichier de log si il n'est pas present
        try {
            messLog.createNewFile();
        } catch (IOException e) {
            e.printStackTrace(System.out);
            System.err.println("Impossible de cree le fichier!");
            System.exit(1);
        }
        //creation du fichier de configuration si il n'est pas present
        messConf = new File(repConf.getPath() + "/config.xml");
        try {
            messConf.createNewFile();
        } catch (IOException e) {
            e.printStackTrace(System.out);
            System.err.println("Impossible de cree le fichier de configuration!");
            System.exit(1);
        }
        ecriture("\n", false);
        //Si l'utilisateur veut changer sa configuration il n'y a "qu'a" changer modifier le fichier de conf.
        if (messConf.length() == 0) {
            //que faire si il n'y as pas de fichier de conf
            //recuperation d'une instance de classe "DocumentBuilderFactory"
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            //creation d'un parseur
            try {
                DocumentBuilder builder = factory.newDocumentBuilder();
                //creation d'un Document
                Document doc = builder.newDocument();
                //creation de l'element racine
                Element racine = doc.createElement("machine");
                doc.appendChild(racine);
                //creation d'un commentaire
                Comment commentaire = doc.createComment("instanciation du minitel");
                racine.appendChild(commentaire); //ajout du commentaire dans le doc XML
                //ajout du peripherique a proprement parler
                Element periphXML = doc.createElement("periph");
                racine.appendChild(periphXML);
                //creation d'un commentaire
                commentaire = doc.createComment("types disponibles: ADM3A, minitel, tvi920c, vt220, wyse");
                periphXML.appendChild(commentaire); //ajout du commentaire dans le doc XML
                commentaire = doc.createComment("prise series disponibles: ttyS0, ttyS1, ttyS2, ttyS4, ttyS5");
                periphXML.appendChild(commentaire); //ajout du commentaire dans le doc XML
                //creation des parametres necessaire a l'instanciation du peripherique
                Element type = doc.createElement("type");
                Element com = doc.createElement("com");
                Element idXML = doc.createElement("id");
                Element name = doc.createElement("name");
                type.appendChild(doc.createTextNode("minitel"));
                com.appendChild(doc.createTextNode("/dev/ttyS1"));
                idXML.appendChild(doc.createTextNode("1"));
                name.appendChild(doc.createTextNode("minitel"));
                periphXML.appendChild(type);
                periphXML.appendChild(com);
                periphXML.appendChild(idXML);
                periphXML.appendChild(name);
                //maintenant  on transforme le resultat pour le mettre en forme avant de l'ecrire dans le fichier
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                //il ne faut pas oublier de configurer le transformer
                transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                DOMSource source = new DOMSource(doc);
                final StreamResult sortie = new StreamResult(messConf);
                transformer.transform(source, sortie);
            } catch (ParserConfigurationException e) {
                System.err.println("erreur de configuration du parser");
                e.printStackTrace(System.err);
            } catch (TransformerConfigurationException e) {
                System.err.println("erreur de configuration du transformer!");
                e.printStackTrace(System.err);
            } catch (TransformerException e) {
                System.err.println("erreur dans le transformer");
                e.printStackTrace(System.err);
            }
        }
        confXML();
    }

    /**
     * Permer d'ecrire dans le fichier de log.
     *
     * @param pMess Contient le message qui doit etre ecrit.
     * @param bDate Contient un booleen pour indiquer si il faut mettre la date
     * au debut du fichier
     */
    public void ecriture(String pMess, boolean bDate) {
        try {
            //On cree un nouveau tampon d'ecriture dans le fichier de log pour pouvoir ecrire dedans
            BufferedWriter bw = new BufferedWriter(new FileWriter(messLog.getPath(), true)); //l'option true permet d'ecrire a la fin du fichier
            if (bDate) {
                SimpleDateFormat sdf = new SimpleDateFormat(); //Crée un nouveau format de date.
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+1")); //Configure le nouveau format comme appartennant au fuseau horaire de Paris.
                bw.write(sdf.format(new Date()) + " " + pMess);
            } else {
                bw.write(pMess); //on marque dans le tampon le message qui devras etre envoyer dans le fichier
            }
            bw.flush(); //on ecrit dans le fichier
            bw.close();
        } catch (IOException e) {
            System.err.println("Impossible d'ecrire dans le fichier de log: " + messLog.getPath());
            System.err.println("Verifier que ce fichier est accessible en ecriture");
            System.err.println("Verifier que le fichier existe");
            System.err.println("vous n'avec probablement pas les droits");
            e.printStackTrace(System.out);
            System.exit(1);
        }
    }

    /**
     * Permet de configurer depuis le fichier de configuration au format XML
     */
    public void confXML() {
        if (isServer) { //que faire si on lance un serialChat de type Server
            //on verifie que l'on est bien dans un environnement de type UNIX/Linux
            osType typeOs = new osType();
            if (typeOs.isUnix()) { //on est bien sur un OS de type linux
                //C'est ici que l'on doit  traiter le contenue du fichier XML pour pouvoir l'executer
                //recuperation d'une instance de classe "DocumentBuilderFactory"
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                try {
                    final DocumentBuilder builder = factory.newDocumentBuilder();
                    final Document doc = builder.parse(messConf);
                    //recuperation de l'element racine c'est a dire "machine"
                    final Element racine = doc.getDocumentElement();
                    //recuperation des periph
                    final NodeList racineNoeuds = racine.getChildNodes();
                    for (int i = 0; i < racineNoeuds.getLength(); i++) {
                        if (racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
                            final Element periphGetXML = (Element) racineNoeuds.item(i);
                            if (periphGetXML.getNodeName().equals("periph")) {
                                //On recupere le nom du peripherique
                                final Element name = (Element) periphGetXML.getElementsByTagName("name").item(0);
                                //On recupere l'ID qui est transmit
                                final Element idGetXML = (Element) periphGetXML.getElementsByTagName("id").item(0);
                                Pattern patId = Pattern.compile("\\s*(\\d{1,2})\\s*");
                                Matcher matchId;
                                if ((matchId = patId.matcher(idGetXML.getTextContent())).matches()) {
                                    System.out.println("l'Id est valide " + idGetXML.getTextContent());
                                } else {
                                    System.err.println("l'Id " + idGetXML.getTextContent() + " n'est pas valide pour le peripherique " + name.getTextContent());
                                    ecriture("l'Id " + idGetXML.getTextContent() + " n'est pas valide pour le peripherique " + name.getTextContent(), true);
                                    System.exit(1);
                                }
                                int IDXML = Integer.parseInt(idGetXML.getTextContent());
                                //on verifie que l'Id est disponnible
                                if (id[IDXML] == null) {
                                    id[IDXML] = name.getTextContent();
                                } else {
                                    System.err.println("L'id " + IDXML + " n'est pas disponible");
                                    ecriture("L'id " + IDXML + " n'est pas disponible", true);
                                    ecriture("On lui attibue un nouvelle ID", true);
                                    IDXML = getNewId(name.getTextContent(), true);
                                }
                                //On recupere aussi le la prise com
                                final Element comGetXML = (Element) periphGetXML.getElementsByTagName("com").item(0);
                                //puis on vas commencer a s'occupper de l'instanciation de l'object
                                final Element typeGetXML = (Element) periphGetXML.getElementsByTagName("type").item(0);
                                boolean isGood = false;
                                if (typeGetXML.getTextContent().equals("tvi920c")) {
                                    isGood = true;
                                    periph[IDXML] = new tvi920c(IDXML, comGetXML.getTextContent());
                                }
                                if (typeGetXML.getTextContent().equals("minitel")) {
                                    isGood = true;
                                    periph[IDXML] = new minitel(IDXML, comGetXML.getTextContent());
                                }
                                if (typeGetXML.getTextContent().equals("ADM3A")) {
                                    isGood = true;
                                    periph[IDXML] = new ADM3A(IDXML, comGetXML.getTextContent());
                                }
                                if (typeGetXML.getTextContent().equals("vt220")) {
                                    isGood = true;
                                    periph[IDXML] = new vt220(IDXML, comGetXML.getTextContent());
                                }
                                if (typeGetXML.getTextContent().equals("wyse")) {
                                    isGood = true;
                                    periph[IDXML] = new wyse(IDXML, comGetXML.getTextContent());
                                }
                                if (isGood) {
                                    periph[IDXML].addObserver(this);
                                } else {
                                    System.exit(1);
                                }
                            }
                        }
                    }
                } catch (final ParserConfigurationException e) {
                    System.err.println("erreur sur le parser");
                    e.printStackTrace(System.err);
                    System.exit(1);
                } catch (SAXException e) {
                    System.err.println("erreur sur le builder du document");
                    e.printStackTrace(System.err);
                    System.exit(1);
                } catch (IOException e) {
                    System.err.println("erreur dans la lecture du fichier de configuration");
                    e.printStackTrace(System.err);
                    System.exit(1);
                }
                //tout les programmes pouvant planter ce situe avant l'instantiation de la partie graphique
                if (allowGui) { //si on a l'autorisation d'afficher l'interface graphique on l'initialise
                    gui = new interfaceGUI(99);
                    gui.addObserver(this); //ajout d'un observateur sur le l'interface graphique
                } else {
                    releaseId(99);
                }
                //on lance le serveur sur le reseau
                netServ = new netServer();
                netServ.addObserver(this);
            } else { //on est pas sur un os de type linux
                System.exit(1);
            }
        }
    }
}
