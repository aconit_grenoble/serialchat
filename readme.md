Réalisation d'une interface entre machines à travers les ages
=============================================================

Ce projet a pour but de créer un serveur de chat entre différent terminaux d'ordinateur de différentes époques.
Il a été mis en oeuvre avec les terminaux suivant :
- DEC VT220
- Minitel
- Decwitter LA-36
- TeleVideo TVI920C
- TI Silent 700
- PC Moderne

Planifiés :
- Decwritter LA-30
- Teletype ASR 33
- Teletype Model 43

Documentation
=============
1. [Introduction](doc/introduction.md)
2. [Outils et Logiciel](doc/outilsEtLogiciels.md)
3. [Programmation](doc/programmation.md)

Crédits 
=======
Georges Schwing
Stage de fin de DUT effectué sous la direction de Philippe Denoyelle